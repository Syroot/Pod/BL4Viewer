# BL4Viewer

This repository hosts the original source of the WitnessTeam (Siklist) "BL4Viewer" tool which can view Pod tracks and
replay ghosts.

## Versions

The `1.41` tag reflects the unmodified version of the tool, as the latest 1.5 source is seemingly lost.

New modifications start at version 1.6.
