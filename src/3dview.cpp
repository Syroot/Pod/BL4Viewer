//**************************************************************************
//* 3dView.cpp	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Définition des fonctions 3D de la classe D3DView
//***************************************************************************

#include "3dview.h"
#include "resource.h"


D3DView::D3DView()
{
	bl4Path = NULL;
	bl4Name = NULL;
	for (int i=0; i<NUMGHT; i++) {
		ghtPath[i] = NULL;
		ghtName[i] = NULL;
		ght[i] = NULL;
	}
	ghtColor[0].a = 0.6f; ghtColor[0].r = 1.0f; ghtColor[0].g = 0.0f; ghtColor[0].b = 0.0f;
	ghtColor[1].a = 0.6f; ghtColor[1].r = 0.0f; ghtColor[1].g = 1.0f; ghtColor[1].b = 0.0f;
	ghtColor[2].a = 0.6f; ghtColor[2].r = 1.0f; ghtColor[2].g = 1.0f; ghtColor[2].b = 0.0f;
	ghtColor[3].a = 0.6f; ghtColor[3].r = 1.0f; ghtColor[3].g = 0.0f; ghtColor[3].b = 1.0f;
	ghtColor[4].a = 0.6f; ghtColor[4].r = 0.0f; ghtColor[4].g = 1.0f; ghtColor[4].b = 1.0f;
	track = NULL;
	d3D=Direct3DCreate9(D3D_SDK_VERSION);
	device = NULL;
	width = 1024;
	height = 768;
	idAdapter = D3DADAPTER_DEFAULT;
	font = new CD3DFont( _T("Arial"), 12, D3DFONT_BOLD );
	texturesPath = (char*) malloc(256);
	if (!GetCurrentDirectory( 256, texturesPath)) { strcpy_s( texturesPath, 256, "c:\\");}
	strcat_s( texturesPath, 256, "\\textures");
	CreateDirectory( texturesPath, NULL);
	ZeroMemory(&mTransfo, sizeof(mTransfo));
	ZeroMemory(&mTemporaire, sizeof(mTemporaire));
	moveStepX = 0.0f;
	moveStepY = 0.0f;
	moveStepZ = 30.0f;
	rotStepZ = 0.0f;
	rotStepX = 60.0f;
	ghtCurrentTime = 0;
	ghtUnPauseTime = 0;
	ghtBeforePauseTime = 0;
	numTraces = 10;
	fpsShowCount = 10;
	ghtPauseTime = 0;
	currentCamera = 0;
	showHelp = true;
	fullScreen = false;
	pause = true;
	active = true;
}

/************************************************************************/

D3DView::~D3DView()
{
	ReleaseAll();
	if (bl4Path) { free(bl4Path); }
	if (bl4Name) { free(bl4Name); }
	for (int i=0; i<NUMGHT; i++) {
		if (ghtPath[i]) { free(ghtPath[i]); }
		if (ghtName[i]) { free(ghtName[i]); }
	}
	free(texturesPath);
	delete(font);
	if (d3D) { d3D->Release(); d3D=NULL; }
}

/************************************************************************/

void D3DView::SetBl4Path( char* path)
{
	if (path != NULL) {
		if (bl4Path) { free(bl4Path); }
		if (bl4Name) { free(bl4Name); }
		bl4Path = path;
		bl4Name = ExtractNameFromPath(bl4Path);
	}
}

/************************************************************************/

void D3DView::SetGhtPath( char* path, int gthId)
{
	if (path != NULL) {
		if (ghtPath[gthId]) { free(ghtPath[gthId]); }
		if (ghtName[gthId]) { free(ghtName[gthId]); }
		ghtPath[gthId] = path;
		ghtName[gthId] = ExtractNameFromPath(ghtPath[gthId]);
	}
}

/************************************************************************/

void D3DView::ResetGhtPath()
{
	for (int i=0; i<NUMGHT; i++) {
		if (ghtPath[i]) {
			free(ghtPath[i]);
			ghtPath[i] = NULL;
			if (ghtName[i]) {
				free(ghtName[i]);
				ghtName[i] = NULL;
			}
		}
	}
}

/************************************************************************/

void D3DView::InitAdapterLists( int adapterList, int modeList)
{
	int numAdapters;
	D3DADAPTER_IDENTIFIER9 adapterIdentifier;

	// Adapter
	numAdapters = d3D->GetAdapterCount();
	for (int i=0; i<numAdapters; i++) {
		d3D->GetAdapterIdentifier(i, 0, &adapterIdentifier);
		SendDlgItemMessage(hDlg, adapterList, CB_ADDSTRING , 0, (LPARAM) (LPCTSTR) adapterIdentifier.Description);
	}
	SendDlgItemMessage(hDlg, adapterList, CB_SETCURSEL  , 0, 0);
	SetAdapter(idAdapter, modeList);
}

/************************************************************************/

void D3DView::SetAdapter(int adapter, int modeList)
{
	idAdapter = adapter;

	// Retrieve current display mode.
	D3DDISPLAYMODE currentMode;
	d3D->GetAdapterDisplayMode(idAdapter, &currentMode);

	// Find all supported modes with same format and refresh rate.
	displayModes.clear();
	int numModes = d3D->GetAdapterModeCount(idAdapter, currentMode.Format);
	displayModes.reserve(numModes);
	int suggestedMode = 0;
	for (int i = 0; i < numModes; i++)
	{
		D3DDISPLAYMODE mode;
		d3D->EnumAdapterModes(idAdapter, currentMode.Format, i, &mode);
		if (mode.RefreshRate == currentMode.RefreshRate)
		{
			displayModes.push_back(mode);

			if (fullScreen)
			{
				if (mode.Width == currentMode.Width && mode.Height == currentMode.Height)
					suggestedMode = (int)displayModes.size() - 1;
			}
			else
			{
				if (mode.Width <= currentMode.Width / 4 * 3 && mode.Height <= currentMode.Height / 4 * 3)
					suggestedMode = (int)displayModes.size() - 1;
			}
		}
	}

	// Fill combobox with available resolutions.
	SendDlgItemMessage(hDlg, modeList, CB_RESETCONTENT, 0, 0);
	for (const auto& mode : displayModes)
	{
		char text[16];
		sprintf_s(text, "%d x %d", mode.Width, mode.Height);
		SendDlgItemMessage(hDlg, modeList, CB_ADDSTRING, 0, (LPARAM)(LPCTSTR)text);
	}
	SendDlgItemMessage(hDlg, modeList, CB_SETCURSEL, suggestedMode, 0);
	SetMode(suggestedMode);
}

/************************************************************************/

void D3DView::SetMode(int mode)
{
	const D3DDISPLAYMODE& displayMode = displayModes[mode];
	width = displayMode.Width;
	height = displayMode.Height;
}

/************************************************************************/

void D3DView::SetFullScreen()
{
	fullScreen = !fullScreen;
}

/************************************************************************/

bool D3DView::SetNumTraces(int tracesBox)
{
	int success;
	numTraces = GetDlgItemInt(hDlg, tracesBox, &success, TRUE);

	if (!success || numTraces <= 0) {
		WriteError("The number of traces must be positive");
		return false;
	}

	return true;
}

/************************************************************************/

void D3DView::Run()
{
	bool cull;
	int i;
	long lastTime, currentTime, elapsedTime;
	HWND hWnd;

	// Création de la fenêtre d3d
	hWnd = CreateD3DWindow();
	D3DInit(hWnd);

	// Chargement du circuit
	track = new Track();
	if (!track->LoadTrack(bl4Path, bl4Name, texturesPath, device)) {
		WriteError("Unable to load circuit");
	} else {
		// Chargement des ghosts
		for (i=0; i<NUMGHT; i++) {
			if (ghtPath[i] != NULL) {
				ght[i] = new Ghost( ghtColor[i], numTraces);
				if (!ght[i]->LoadGht( ghtPath[i])) { WriteError("Unable to load ghost"); delete ght[i]; ght[i] = NULL;}
				else {
					ght[i]->Init( device, i );
					ght[i]->EnableTraces(true);
					ght[i]->EnableInnerLight(device, true);
				}
			}
		}

		// Initialisation des variables
		ghtUnPauseTime = timeGetTime();
		ghtPauseTime = timeGetTime();
		lastTime = timeGetTime()-100;
		ghtCurrentTime = 0;
		ghtUnPauseTime = 0;
		ghtBeforePauseTime = 0;
		showHelp = true;
		pause = true;
		run = true;
		cull = false;
		active = true;

		// Boucle principale
		MSG msg;
		while(run)
		{
			while (PeekMessage(&msg,NULL,0,0,PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			// Calcul fps
			currentTime = timeGetTime();
			elapsedTime = currentTime - lastTime;
			if ( elapsedTime > 0) {
				lastTime = currentTime;
				if ( fpsShowCount == 10 ) {
					fps = 1000 / elapsedTime;
					_itoa_s( fps, fpsStr, 10);
					fpsShowCount = 0;
				}
				fpsShowCount++;

				// Lecture entrées
				if (active || fullScreen)
				{
					if (currentCamera==0) {
						// Caméra libre
						if (GetAsyncKeyState(VK_SHIFT)) {
							if (GetAsyncKeyState(VK_LEFT))	rotStepZ  += 0.05f * elapsedTime;
							if (GetAsyncKeyState(VK_RIGHT)) rotStepZ  -= 0.05f * elapsedTime;
							if (GetAsyncKeyState(VK_DOWN))	rotStepX  += 0.05f * elapsedTime;
							if (GetAsyncKeyState(VK_UP))	rotStepX  -= 0.05f * elapsedTime;
						} else {
							if (GetAsyncKeyState(VK_LEFT))	moveStepX += 0.005f * elapsedTime;
							if (GetAsyncKeyState(VK_RIGHT))	moveStepX -= 0.005f * elapsedTime;
							if (GetAsyncKeyState(VK_DOWN))	moveStepY += 0.005f * elapsedTime;
							if (GetAsyncKeyState(VK_UP))	moveStepY -= 0.005f * elapsedTime;
						}
						if (GetAsyncKeyState(VK_SUBTRACT))	moveStepZ += 0.005f * elapsedTime;
						if (GetAsyncKeyState(VK_ADD))		moveStepZ -= 0.005f * elapsedTime;
					} else {
						// Caméra vérouillée
						if (GetAsyncKeyState(VK_LEFT))						rotStepZ  -= 0.05f * elapsedTime;
						if (GetAsyncKeyState(VK_RIGHT))						rotStepZ  += 0.05f * elapsedTime;
						if (GetAsyncKeyState(VK_DOWN) && rotStepX<=179)		rotStepX  += 0.05f * elapsedTime;
						if (GetAsyncKeyState(VK_UP)	  && rotStepX>=1)		rotStepX  -= 0.05f * elapsedTime;
						if (GetAsyncKeyState(VK_SUBTRACT))					moveStepZ -= 0.0025f * elapsedTime;
						if (GetAsyncKeyState(VK_ADD)  && moveStepZ<=-0.3)	moveStepZ += 0.0025f * elapsedTime;
					}

					if ((GetAsyncKeyState('0') | GetAsyncKeyState(VK_NUMPAD0)) & 0x00000001) {
						// Sélection mode caméra libre
						ChangeCamera(0);
					}
					if ((GetAsyncKeyState('1') | GetAsyncKeyState(VK_NUMPAD1)) & 0x00000001) {
						// Sélection mode caméra vérouillée sur ghost 1
						if (ght[0]) ChangeCamera(currentCamera == 1 ? 6 : 1);
					}
					if ((GetAsyncKeyState('2') | GetAsyncKeyState(VK_NUMPAD2)) & 0x00000001) {
						// Sélection mode caméra vérouillée sur ghost 2
						if (ght[1]) ChangeCamera(currentCamera == 2 ? 7 : 2);
					}
					if ((GetAsyncKeyState('3') | GetAsyncKeyState(VK_NUMPAD3)) & 0x00000001) {
						// Sélection mode caméra vérouillée sur ghost 3
						if (ght[2]) ChangeCamera(currentCamera == 3 ? 8 : 3);
					}
					if ((GetAsyncKeyState('4') | GetAsyncKeyState(VK_NUMPAD4)) & 0x00000001) {
						// Sélection mode caméra vérouillée sur ghost 4
						if (ght[3]) ChangeCamera(currentCamera == 4 ? 9 : 4);
					}
					if ((GetAsyncKeyState('5') | GetAsyncKeyState(VK_NUMPAD5)) & 0x00000001) {
						// Sélection mode caméra vérouillée sur ghost 5
						if (ght[4]) ChangeCamera(currentCamera == 5 ? 10 : 5);
					}
					if (GetAsyncKeyState('T') & 0x00000001) {
						// Affichage Traces
						for (i=0; i<NUMGHT; i++) {
							if (ght[i]) ght[i]->EnableTraces(!ght[i]->IsTracesOn());
						}
					}
					if (GetAsyncKeyState('L') & 0x00000001) {
						// Affichage Lumières
						for (i=0; i<NUMGHT; i++) {
							if (ght[i]) ght[i]->EnableInnerLight(device, !ght[i]->IsLightOn());
						}
					}
					if (GetAsyncKeyState('P') & 0x00000001) {
						// Lecture / Pause
						if (pause) {
							// Passe en lecture
							ghtUnPauseTime = timeGetTime();
						} else {
							// Passe en pause
							ghtBeforePauseTime = ghtCurrentTime;
							ghtPauseTime = timeGetTime();
						}
						pause = !pause;
					}
					if (GetAsyncKeyState('R') & 0x00000001) {
						// Lecture remise à 0
						pause = true;
						ghtCurrentTime = 0;
						ghtBeforePauseTime = 0;
						ghtUnPauseTime = 0;
						for (int i=0; i<NUMGHT; i++) {
							if (ght[i]) ght[i]->Rewind();
						}
					}
					if (GetAsyncKeyState('M') & 0x00000001) {
						mirrors = !mirrors;
					}
					if (GetAsyncKeyState(VK_F1) & 0x00000001)		{
						// Affichage Aide
						showHelp = !showHelp;
					}
					if (GetAsyncKeyState(VK_SPACE) & 0x00000001)	{
						// Rendu 2 faces
						if (cull) {
							device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW );
							cull = !cull;
						} else {
							device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE );
							cull = !cull;
						}
					}
					if (GetAsyncKeyState(VK_ESCAPE)) run = false;
				}

				// Calculs ghosts
				if (ght[0]) {
					if (!pause) {
						ghtCurrentTime = ghtBeforePauseTime + timeGetTime() - ghtUnPauseTime;
					}
					sprintf_s(ghtCurrentTimeStr, "%d'%02d''%03d", ghtCurrentTime / 60000, (ghtCurrentTime / 1000) % 60, ghtCurrentTime % 1000);
				}

				// Affichage
				Render();

			}	// (if elapsedTime>0)
		}	// while(run)
	}	// else if(!track->LoadTrack())

	ReleaseAll();
	DestroyWindow(hWnd);
}

/************************************************************************/

LRESULT CALLBACK D3DView::WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static D3DView* inst;

	switch (msg)
	{
		case WM_CREATE:
		{
			LPCREATESTRUCT createStruct = (LPCREATESTRUCT)lParam;
			inst = (D3DView*)createStruct->lpCreateParams;
			break;
		}
		case WM_DESTROY:
		{
			inst->run = false;
			break;
		}
		case WM_ACTIVATEAPP:
		{
			inst->active = (bool)wParam;
			break;
		}
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

/************************************************************************/

HWND D3DView::CreateD3DWindow()
{
	HWND hWnd;

	WNDCLASS wc;
	wc.style = CS_OWNDC;
	wc.lpfnWndProc = WindowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_BL4VIEWER));
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "D3DView";
	RegisterClass(&wc);

	DWORD ws = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU;

	// Create window with appropriate size.
	if (fullScreen)
	{
		hWnd = CreateWindow("D3DView", "Visualization", ws, 0, 0, width, height, hDlg, NULL, hInst, this);
	}
	else
	{
		RECT rect{ 0, 0, width, height };
		AdjustWindowRectEx(&rect, ws, FALSE, 0);
		DWORD cw = rect.right - rect.left;
		DWORD ch = rect.bottom - rect.top;
		hWnd = CreateWindow("D3DView", "Visualization", ws, CW_USEDEFAULT, CW_USEDEFAULT, cw, ch, hDlg, NULL, hInst, this);
	}
	ShowWindow(hWnd, SW_SHOW);

	return hWnd;
}

/************************************************************************/

void D3DView::D3DInit(HWND hWnd)
{
	// Création du device
	D3DDISPLAYMODE displayMode;
	d3D->GetAdapterDisplayMode(idAdapter,&displayMode);
	// Réso d'affichage.  Pour la taille de la fenetre, cf à la création (createwindows)
	displayMode.Width = width;displayMode.Height = height;

	D3DPRESENT_PARAMETERS presentParameters;

	presentParameters.BackBufferWidth = displayMode.Width;
	presentParameters.BackBufferHeight = displayMode.Height;
	presentParameters.BackBufferFormat = displayMode.Format;
	presentParameters.BackBufferCount = 1;
	presentParameters.MultiSampleType = D3DMULTISAMPLE_NONE;
	presentParameters.MultiSampleQuality = 0;
	presentParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	presentParameters.hDeviceWindow = hWnd;
	presentParameters.Windowed = !fullScreen;
	presentParameters.EnableAutoDepthStencil = TRUE;
	presentParameters.Flags = 0;
	presentParameters.FullScreen_RefreshRateInHz = 0;
	presentParameters.PresentationInterval = fullScreen ? D3DPRESENT_INTERVAL_IMMEDIATE : D3DPRESENT_INTERVAL_DEFAULT;

	// Determine stencil support.
	if (FAILED(d3D->CheckDeviceFormat(idAdapter, D3DDEVTYPE_HAL, displayMode.Format, D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, D3DFMT_D24S8))
		|| FAILED(d3D->CheckDepthStencilMatch(idAdapter, D3DDEVTYPE_HAL, displayMode.Format, displayMode.Format, D3DFMT_D24S8)))
	{
		if (displayMode.Format == D3DFMT_R5G6B5)
			MessageBox(hDlg, "Mirror effect disabled, switch to 32bit color mode next time", "Mirror effect disabled", MB_ICONWARNING);
		else
			MessageBox(hDlg, "Mirror effect disabled, stencil buffer is not supported", "Mirror effect disabled", MB_ICONWARNING);
		presentParameters.AutoDepthStencilFormat = D3DFMT_D16;
		stencil = false;
	}
	else
	{
		presentParameters.AutoDepthStencilFormat = D3DFMT_D24S8;
		stencil = 1;
	}

	if (d3D->CreateDevice(idAdapter, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &presentParameters, &device) != D3D_OK) {
		// Pas de device HAL
		WriteError("Hardware acceleration impossible");
		ReleaseAll();
		DestroyWindow(hWnd);
		exit(0);
	}

	// Transformations initiales
		// Projection
	D3DXMatrixPerspectiveFovLH(&mTransfo, D3DX_PI/2, width/(float)height, 0.001f, 500.0f);
	device->SetTransform(D3DTS_PROJECTION, &mTransfo);
		// World
	D3DXMatrixIdentity(&mTransfo);
	device->SetTransform(D3DTS_WORLD, &mTransfo);
		// View
	SetCamera();

	font->InitDeviceObjects( device );
	font->RestoreDeviceObjects();

	// Renderstates globaux
	device->SetRenderState(D3DRS_LIGHTING, TRUE);
	device->SetRenderState(D3DRS_SPECULARENABLE, TRUE);
	device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW );
	device->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_ARGB(255, 255, 255, 255));
	device->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE , D3DMCS_COLOR1);
	device->SetRenderState(D3DRS_SPECULARMATERIALSOURCE , D3DMCS_COLOR1);
	device->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE , D3DMCS_COLOR1);
	device->SetRenderState(D3DRS_DITHERENABLE, TRUE);
	device->SetRenderState(D3DRS_ZENABLE, TRUE);
	device->SetSamplerState(0,D3DSAMP_MAGFILTER,D3DTEXF_LINEAR);
}

/************************************************************************/

void D3DView::Render()
{
	// Nettoyage
	device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0,0,0), 1.0f, 0);
	if (stencil)
		device->Clear(0, NULL, D3DCLEAR_STENCIL, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	// Transformation VIEW
	D3DXMatrixIdentity(&mTransfo);
	device->SetTransform(D3DTS_WORLD, &mTransfo);
	SetCamera();

	for (int i = 0; i < NUMGHT; i++)
		if (ght[i])
			ght[i]->Update(device, ghtCurrentTime + ght[i]->GetRaceStartTime());

	// Dessin de la scène
	device->BeginScene();

	// Circuit
	if (track) {
		track->Draw(device);
	}

	bool flashText = timeGetTime() % 1000 < 500;
	float yTextPos = (float)height - 20;

	// Draw replay time.
	if (flashText || !pause)
		font->DrawText((float)width - 100, yTextPos, D3DCOLOR_ARGB(150, 255, 0, 0), ghtCurrentTimeStr);
	yTextPos -= 20;

	// Ghosts
	int focusedGhost = currentCamera - 1;
	if (focusedGhost >= NUMGHT)
		focusedGhost -= NUMGHT;

	for (int i=0; i<NUMGHT; i++) {
		if (!ght[i])
			break;

		if (currentCamera - 1 - NUMGHT != i)
			ght[i]->Draw(device);

		// Texte clignote si caméra vérouillée sur ghost i
		if (flashText || focusedGhost != i)
			font->DrawText((float)width - 100, yTextPos, ght[i]->GetColor(), ght[i]->GetPilotName());
		yTextPos -= 17;
	}

	// Textes généraux
	if (showHelp)
	{
		font->DrawText((float)width - 80, 0, D3DCOLOR_ARGB(150, 255, 255, 0), fpsStr);
		font->DrawText((float)width - 35, 0, D3DCOLOR_ARGB(150, 255, 255, 0), "fps");

		font->DrawText(2,  0, D3DCOLOR_ARGB(255, 255, 128, 0), bl4Name);
		font->DrawText(2, 15, D3DCOLOR_ARGB(150, 255, 128, 0), track->GetNumFaces());
		font->DrawText(2, 30, D3DCOLOR_ARGB(150, 255, 128, 0), track->GetNumTextures());

		DWORD keyColor = D3DCOLOR_ARGB(150, 0, 100, 255);
		font->DrawText(  2, (float)height - 185, keyColor, "F1");
		font->DrawText(115, (float)height - 185, keyColor, "= infos");
		font->DrawText(  2, (float)height - 170, keyColor, "0,1,2...");
		font->DrawText(115, (float)height - 170, keyColor, "= cameras");
		font->DrawText(  2, (float)height - 155, keyColor, "Arrows");
		font->DrawText(115, (float)height - 155, keyColor, "= move");
		font->DrawText(  2, (float)height - 140, keyColor, "Shift + Arrows");
		font->DrawText(115, (float)height - 140, keyColor, "= rotate");
		font->DrawText(  2, (float)height - 125, keyColor, "+/-");
		font->DrawText(115, (float)height - 125, keyColor, "= zoom");
		font->DrawText(  2, (float)height - 110, keyColor, "R");
		font->DrawText(115, (float)height - 110, keyColor, "= restart");
		font->DrawText(  2, (float)height -  95, keyColor, "P");
		font->DrawText(115, (float)height -  95, keyColor, "= play / pause");
		font->DrawText(  2, (float)height -  80, keyColor, "L");
		font->DrawText(115, (float)height -  80, keyColor, "= lights");
		font->DrawText(  2, (float)height  - 65, keyColor, "M");
		font->DrawText(115, (float)height  - 65, keyColor, "= mirrors");
		font->DrawText(  2, (float)height  - 50, keyColor, "T");
		font->DrawText(115, (float)height  - 50, keyColor, "= traces");
		font->DrawText(  2, (float)height -  35, keyColor, "Space");
		font->DrawText(115, (float)height -  35, keyColor, "= culling");
		font->DrawText(  2, (float)height -  20, keyColor, "Escape");
		font->DrawText(115, (float)height -  20, keyColor, "= quit");
	}

	device->EndScene();
	device->Present(NULL, NULL, NULL, NULL);
}

/************************************************************************/

void D3DView::ReleaseAll()
{
	if (track) { delete track; track = NULL; device->SetTexture(0, NULL);}
	for (int i=0; i<NUMGHT; i++) {
		if (ght[i]) { delete ght[i]; ght[i] = NULL; }
	}
	if (font) { font->InvalidateDeviceObjects(); font->DeleteDeviceObjects(); }
	if (device) { device->Release(); device=NULL; }
	moveStepX = 0.0f;
	moveStepY = 0.0f;
	moveStepZ = 30.0f;
	rotStepZ = 0.0f;
	rotStepX = 60.0f;
	currentCamera = 0;
}

/************************************************************************/

void D3DView::WriteError(const char* msg) const
{
	MessageBox(hDlg, msg,"Error",MB_ICONERROR);
}

/************************************************************************/

void D3DView::ChangeCamera(int newCamera)
{
	if (newCamera != currentCamera) {
		if (newCamera == 0) {
			// Caméra libre
			moveStepX = 0.0f;
			moveStepY = 0.0f;
			moveStepZ = 30.0f;
			rotStepZ = 0.0f;
			rotStepX = 60.0f;
		} else if (currentCamera == 0) {
			// Caméra vérouillée sur ghost. On ne réinitialise les params que si la caméra précédente était la libre
			moveStepX = 0.0f;
			moveStepY = 0.0f;
			moveStepZ = -10.0f;		// Distance initiale entre caméra et cible
			rotStepZ = 0.0f;
			rotStepX = 1.0f;
		}
		currentCamera = newCamera;
	}
}

/************************************************************************/

void D3DView::SetCamera()
{
	D3DXMatrixIdentity(&mTransfo);

	if (currentCamera == 0) {
		// Caméra libre
		D3DXMatrixRotationZ(&mTemporaire,D3DXToRadian(rotStepZ));
		D3DXMatrixMultiply(&mTransfo,&mTransfo,&mTemporaire);
		D3DXMatrixRotationX(&mTemporaire,D3DXToRadian(rotStepX));
		D3DXMatrixMultiply(&mTransfo,&mTransfo,&mTemporaire);
		D3DXMatrixTranslation(&mTemporaire,moveStepX,moveStepY,moveStepZ);
		D3DXMatrixMultiply(&mTransfo,&mTransfo,&mTemporaire);
	} else if (currentCamera <= NUMGHT) {
		// Caméra vérouillée sur ghost
		D3DXMatrixRotationY(&mTemporaire, D3DXToRadian(rotStepX));
		D3DXMatrixMultiply(&mTransfo, &mTransfo, &mTemporaire);
		D3DXMatrixRotationZ(&mTemporaire, D3DXToRadian(rotStepZ));
		D3DXMatrixMultiply(&mTransfo, &mTransfo, &mTemporaire);

		D3DXVECTOR3 pos(0.0f, 0.0f, moveStepZ);
		D3DXVECTOR4 posT;
		D3DXVec3Transform(&posT, &pos, &mTransfo);

		GHTPOINT ghtPoint = *ght[currentCamera - 1]->GetGhtPoint();
		ghtPoint.gPoint.z = -ghtPoint.gPoint.z;
		D3DXVECTOR3 eye(
			ghtPoint.gPoint.x + posT.x,
			ghtPoint.gPoint.y + posT.y,
			ghtPoint.gPoint.z + posT.z);
		D3DXVECTOR3 up(0.0f, 0.0f, -1.0f);
		D3DXMatrixLookAtLH(&mTransfo, &eye, (D3DXVECTOR3*)&ghtPoint.gPoint, &up);
	} else {
		// Cockpit ghost camera
		GHTPOINT ghtPoint = *ght[currentCamera - 1 - NUMGHT]->GetGhtPoint();
		D3DXVECTOR3 eye(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 pos(
			ghtPoint.yAxis.z * ghtPoint.xAxis.y - ghtPoint.yAxis.y * ghtPoint.xAxis.z,
			ghtPoint.yAxis.x * ghtPoint.xAxis.z - ghtPoint.yAxis.z * ghtPoint.xAxis.x,
			ghtPoint.yAxis.y * ghtPoint.xAxis.x - ghtPoint.xAxis.y * ghtPoint.yAxis.x);
		D3DXVECTOR3 up(pos.x, pos.y, -pos.z);
		ghtPoint.xAxis.z = -ghtPoint.xAxis.z;
		D3DXMatrixLookAtLH(&mTransfo, &eye, (D3DXVECTOR3*)&ghtPoint.xAxis, &up);

		D3DXMatrixTranslation(&mTemporaire, -ghtPoint.gPoint.x, -ghtPoint.gPoint.y, ghtPoint.gPoint.z + 0.027f);
		D3DXMatrixMultiply(&mTransfo, &mTemporaire, &mTransfo);
	}

	device->SetTransform(D3DTS_VIEW, &mTransfo);
}
