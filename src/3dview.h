//**************************************************************************
//* 3dView.h	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Gére la fenêtre de visualisation 3D du circuit
//***************************************************************************


#ifndef __3DVIEW_H
#define __3DVIEW_H

#include <d3d9.h>
#include <d3dx9.h>
#include "d3dfont.h"
#include "stdio.h"
#include <time.h>
#include "misc.h"
#include "mmsystem.h"
#include "struct3d.h"
#include "track.h"
#include "ghost.h"
#include <vector>

#define NUMGHT 5									// Nombre de ghosts autorisés


class D3DView {
public:
	D3DView();
	~D3DView();

	void Run();										// Lance la visualisation 3D
	void SetBl4Path( char* path);					// Initialisation des path et nom du bl4
	void SetGhtPath( char* path, int gthId);		// Initialisation des path et nom du ghost dont l'index est en paramètre
	void ResetGhtPath();							// Libération des path et nom du ghost
	void SetAdapter( int adapter, int modeList);	// Choix de la carte vidéo
	void SetMode( int mode);						// Choix de la résolution
	void InitAdapterLists( int adapterList, int modeList);	// Initialisation de la liste des cartes vidéos et des modes
	void SetFullScreen();							// Choix plein écran ou non
	bool SetNumTraces(int tracesBox);				// Choix du nombre de traces des ghosts


	// Méthodes d'accès
	inline const char* GetBl4Name() const			{ return bl4Name; }
	inline const char* GetGhtName(int ghtId) const	{ return ghtName[ghtId]; }
	inline int GetNumTraces() const					{ return numTraces;}
	inline void SetHInstance( HINSTANCE hi)			{ hInst = hi; }
	inline void SetHDlg( HWND hd)					{ hDlg = hd; }

private:
	// Méthodes privées
	static LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	HWND CreateD3DWindow();					// Crée la fenêtre de visualisation
	void D3DInit(HWND hWnd);				// Initialisation de l'affichage D3D
	void Render();							// Rendering
	void ReleaseAll();						// Libération mémoire de tous les objets et réinitialisation des paramètres de la scène
	void WriteError(const char* msg) const;	// Affiche le message d'erreur msg
	void ChangeCamera(int newCamera);		// Sélectionne la nouvelle caméra si différente de l'actuelle
	void SetCamera();						// Applique la transformation VIEW correspondant à la caméra sélectionnée

	// Champs
	HINSTANCE hInst;				// Handle de l'application
	HWND hDlg;						// Handle de la fenêtre parent (boite de dialogue)
	char* texturesPath;				// Chemin complet du répertoire textures
	char* bl4Path;					// Chemin complet du bl4
	char* bl4Name;					// Nom du bl4
	char* ghtPath[NUMGHT];			// Chemins complets des ghosts
	char* ghtName[NUMGHT];			// Noms des ghosts
	long ghtUnPauseTime;			// Horaire du dernier unpause
	long ghtBeforePauseTime;		// Temps écoulé jusqu'à la dernière pause
	long ghtCurrentTime;			// Temps courant du ghost
	char ghtCurrentTimeStr[10];		// Idem en caractères
	long ghtPauseTime;				// Horaire du dernier pause (utilisé pour faire clignoter l'affichage du temps)
	int fps;						// Nb d'images / seconde
	char fpsStr[5];					// Idem en caractères
	int fpsShowCount;				// Compteur pour ne calculer les fps qu'à intervalles réguliers (=> affichage stable)
	bool showHelp;					// Indique s'il faut afficher le texte d'aide
	bool fullScreen;				// Indique si le mode plein écran est demandé
	bool mirrors;					// Whether to render mirror effects
	bool pause;						// Indique si la lecture des ghosts est en pause
	bool run;						// Whether to keep running the render loop or exit it
	bool active;					// Whether the window is currently focused
	int currentCamera;				// Caméra selectionnée : 0 = libre, 1,2... = vérouillée sur le ghost correpondant
	Track* track;					// Circuit
	Ghost* ght[NUMGHT];				// Ghosts
	D3DCOLORVALUE ghtColor[NUMGHT];	// Couleurs des ghosts
	int numTraces;					// Nombre de traces des ghosts
	int idAdapter;					// Carte graphique choisie
	std::vector<D3DDISPLAYMODE>	displayModes; // List of available resources of chosen adapter
	int width;						// Pour la réso d'affichage 3d
	int height;						// ...
	bool stencil;					// Whether to clear the stencil buffer
	CD3DFont*  font;				// Font pour l'affichage dans la fenêtre 3D
	float moveStepX;				// Pas pour la translation X
	float moveStepY;				// Pas pour la translation Y
	float moveStepZ;				// Pas pour la translation Z
	float rotStepX;					// Pas pour la rotation X
	float rotStepZ;					// Pas pour la rotation Z

	IDirect3D9 *d3D;				// Interface D3D8
	IDirect3DDevice9 *device;		// Device en cours
	D3DXMATRIX mTransfo;			// Matrice de transformation
	D3DXMATRIX mTemporaire;			// Matrice pour les calculs temporaires
};

#endif // !defined
