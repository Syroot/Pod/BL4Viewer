========================================================================
	   WIN32 APPLICATION : bl4viewer
========================================================================


AppWizard has created this bl4viewer application for you.

This file contains a summary of what you will find in each of the files that
make up your bl4viewer application.

bl4viewer.cpp
	This is the main application source file.

bl4viewer.dsp
	This file (the project file) contains information at the project level and
	is used to build a single project or subproject. Other users can share the
	project (.dsp) file, but they should export the makefiles locally.


/////////////////////////////////////////////////////////////////////////////
AppWizard has created the following resources:

bl4viewer.rc
	This is a listing of all of the Microsoft Windows resources that the
	program uses.  It includes the icons, bitmaps, and cursors that are stored
	in the RES subdirectory.  This file can be directly edited in Microsoft
	Visual C++.

res\bl4viewer.ico
	This is an icon file, which is used as the application's icon (32x32).
	This icon is included by the main resource file bl4viewer.rc.

small.ico
	%%This is an icon file, which contains a smaller version (16x16)
	of the application's icon. This icon is included by the main resource
	file bl4viewer.rc.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
	These files are used to build a precompiled header (PCH) file
	named bl4viewer.pch and a precompiled types file named StdAfx.obj.

Resource.h
	This is the standard header file, which defines new resource IDs.
	Microsoft Visual C++ reads and updates this file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.


/////////////////////////////////////////////////////////////////////////////

Version : 1.6 (Ray)

add support for MSVC14 and x64 builds
add support for Direct3D9
add support for any resolution
double the field of view
fix window not opening with size of selected resolution
disable full screen by default
disable F1 info / help timer
disable inactive window keyboard input
translate UI to English


Version : 1.5 (reversed)

added camera cockpit view
added support for mirror effects (not yet implemented)


Version : 1.41

correction bug tentative de rendu d'un vertexbuffer nul (cas ou le track possède des textures sans les utiliser)
correction vitesse d'affichage du backbuffer en plein écran


Version : 1.4

correction bug de perspective ratio
caméras vérouillées sur ghost
gestion 5 ghosts
utilisation de primitives indexées pour le rendu du circuit


Version : 1.34

correction du bug 1000 fps
gestion du format de couleur 16b / 32b de windows


Version : 1.33

halo lumineux sur les ghosts


Version : 1.32

affichage des fps
stabilisation de la vitesse des mouvements
affichage de 2 ghosts avec gestion pause/lecture/redémarrer et traces


Version : 1.2

mode 1600*1200 ajouté
modification des mouvements de caméras
disparition de tous les textes écrans au bout de 10s
