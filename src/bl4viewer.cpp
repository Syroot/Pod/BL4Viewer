//**************************************************************************
//* bl4viewer.cpp	- bl4 Viewer
//*
//* Picpic
//*
//* 12 Janvier 2003
//*
//* Module principal : création des boites de dialogue et lancement de l'application
//***************************************************************************


#include "bl4viewer.h"

// Variables globales
HINSTANCE hInst;								// current instance
const char* szWindowClass = "BL4VIEWER";		// window class name
D3DView view;


// Foward declarations of functions included in this code module
ATOM				MyRegisterClass(HINSTANCE hInstance);
LRESULT CALLBACK	MainBoxProc(HWND, UINT, WPARAM, LPARAM);



/************************************************************************/

// Point d'entrée de l'application
int APIENTRY WinMain(HINSTANCE hInstance,
					 HINSTANCE hPrevInstance,
					 LPSTR     lpCmdLine,
					 int       nCmdShow)
{

	MyRegisterClass(hInstance);
	hInst = hInstance; // Store instance handle in our global variable

	// Création de la fenêtre principale (boite de dialogue)
	DialogBox(hInst, (LPCTSTR)IDD_MAINBOX, NULL, (DLGPROC)MainBoxProc);

	return 0;
}

/************************************************************************/

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)MainBoxProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_BL4VIEWER);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

/************************************************************************/

// Procédure de traitement des messages de la boite de dialogue principale
LRESULT CALLBACK MainBoxProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;

	switch (message)
	{
		case WM_INITDIALOG:
			view.SetHInstance(hInst);
			view.SetHDlg(hDlg);
			view.InitAdapterLists(IDC_ADAPTER, IDC_MODE);
			SetDlgItemInt(hDlg, IDC_TRACES, view.GetNumTraces(), TRUE);
			return TRUE;
		case WM_COMMAND:
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);

			switch (wmId)
			{
			case IDC_MODE:
				if (wmEvent==CBN_SELCHANGE) {
					view.SetMode((int)SendDlgItemMessage(hDlg, IDC_MODE, CB_GETCURSEL , 0, 0));
				}
				break;
			case IDC_ADAPTER:
				if (wmEvent==CBN_SELCHANGE) {
					view.SetAdapter((int)SendDlgItemMessage(hDlg, IDC_ADAPTER, CB_GETCURSEL , 0, 0), IDC_MODE);
				}
				break;
			case IDOPENBL4:
				view.SetBl4Path(OpenSaveFile(hDlg, false, "Bl4 Files (*.bl4)\0*.bl4\0\0" , "bl4"));
				SetWindowText(GetDlgItem(hDlg, IDC_BL4NAME), view.GetBl4Name());
				view.ResetGhtPath();
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME), "");
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME2), "");
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME3), "");
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME4), "");
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME5), "");
				EnableWindow(GetDlgItem(hDlg, IDC_STATICGHT), true);
				EnableWindow(GetDlgItem(hDlg, IDOPENGHT), true);
				EnableWindow(GetDlgItem(hDlg, IDOPENGHT2), false);
				EnableWindow(GetDlgItem(hDlg, IDOPENGHT3), false);
				EnableWindow(GetDlgItem(hDlg, IDOPENGHT4), false);
				EnableWindow(GetDlgItem(hDlg, IDOPENGHT5), false);
				EnableWindow(GetDlgItem(hDlg, IDC_STATICTRACES), false);
				EnableWindow(GetDlgItem(hDlg, IDC_TRACES), false);
				if (view.GetBl4Name() != NULL) EnableWindow(GetDlgItem(hDlg, IDVIEW), true);
				UpdateWindow(hDlg);
				break;
			case IDOPENGHT:
				view.SetGhtPath(OpenSaveFile(hDlg, false, "Ghost Files (*.seq, *.ght)\0*.seq;*.ght\0\0" , "seq"), 0);
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME), view.GetGhtName(0));
				if (view.GetGhtName(0)) {
					EnableWindow(GetDlgItem(hDlg, IDC_STATICTRACES), true);
					EnableWindow(GetDlgItem(hDlg, IDC_TRACES), true);
					EnableWindow(GetDlgItem(hDlg, IDOPENGHT2), true);
				}
				UpdateWindow(hDlg);
				break;
			case IDOPENGHT2:
				view.SetGhtPath(OpenSaveFile(hDlg, false, "Ghost Files (*.seq, *.ght)\0*.seq;*.ght\0\0" , "seq"), 1);
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME2), view.GetGhtName(1));
				if (view.GetGhtName(1)) {
					EnableWindow(GetDlgItem(hDlg, IDOPENGHT3), true);
				}
				UpdateWindow(hDlg);
				break;
			case IDOPENGHT3:
				view.SetGhtPath(OpenSaveFile(hDlg, false, "Ghost Files (*.seq, *.ght)\0*.seq;*.ght\0\0" , "seq"), 2);
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME3), view.GetGhtName(2));
				if (view.GetGhtName(2)) {
					EnableWindow(GetDlgItem(hDlg, IDOPENGHT4), true);
				}
				UpdateWindow(hDlg);
				break;
			case IDOPENGHT4:
				view.SetGhtPath(OpenSaveFile(hDlg, false, "Ghost Files (*.seq, *.ght)\0*.seq;*.ght\0\0" , "seq"), 3);
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME4), view.GetGhtName(3));
				if (view.GetGhtName(3)) {
					EnableWindow(GetDlgItem(hDlg, IDOPENGHT5), true);
				}
				UpdateWindow(hDlg);
				break;
			case IDOPENGHT5:
				view.SetGhtPath(OpenSaveFile(hDlg, false, "Ghost Files (*.seq, *.ght)\0*.seq;*.ght\0\0" , "seq"), 4);
				SetWindowText(GetDlgItem(hDlg, IDC_GHTNAME5), view.GetGhtName(4));
				UpdateWindow(hDlg);
				break;
			case IDC_FULLSCREEN:
				view.SetFullScreen();
				view.SetAdapter((int)SendDlgItemMessage(hDlg, IDC_ADAPTER, CB_GETCURSEL , 0, 0), IDC_MODE);
				break;
			case IDVIEW:
				if (IsWindowEnabled(GetDlgItem(hDlg, IDC_TRACES))) {
					if( !view.SetNumTraces(IDC_TRACES)) break;
				}
				EnableWindow(GetDlgItem(hDlg, IDVIEW), false);
				view.Run();
				EnableWindow(GetDlgItem(hDlg, IDVIEW), true);
				break;
			case IDCANCEL:
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
   }
   return FALSE;
}
