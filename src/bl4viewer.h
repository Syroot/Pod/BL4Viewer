//**************************************************************************
//* bl4viewer.h	- bl4 Viewer
//*
//* Picpic
//*
//* 25 Août 2002
//*
//* Module principal
//***************************************************************************


#if !defined(AFX_BL4VIEWER_H__C2C2C7CC_B839_11D6_A529_C0CCB2C1FFFF__INCLUDED_)
#define AFX_BL4VIEWER_H__C2C2C7CC_B839_11D6_A529_C0CCB2C1FFFF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h>
#include <stdlib.h>
#include "resource.h"
#include "misc.h"
#include "3dview.h"

#pragma comment(linker, "\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
	processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#endif // !defined(AFX_BL4VIEWER_H__C2C2C7CC_B839_11D6_A529_C0CCB2C1FFFF__INCLUDED_)
