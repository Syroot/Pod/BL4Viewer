//**************************************************************************
//* ghost.h	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Définition des fonctions de gestion d'un ghost
//***************************************************************************

#include "ghost.h"

#define FIX2MS(dw) (long)(dw / 65.536)

Ghost::Ghost(D3DCOLORVALUE col,int nTraces) : numTraces(nTraces)
{
	ghtFile = NULL;
	ghtDatas = NULL;
	vBGht = NULL;
	ghtVertices = NULL;
	currentGhtPoint = 0;
	tracesOn = true;
	lightOn = true;
	subAlphaTrace = 0;
	raceStartTime = 0;
	ghtPoint = {};
	color = D3DCOLOR_COLORVALUE(col.r,col.g,col.b,col.a);
	innerLight.Type = D3DLIGHT_POINT;
	innerLight.Diffuse.r  = col.r;
	innerLight.Diffuse.g  = col.g;
	innerLight.Diffuse.b  = col.b;
	innerLight.Ambient.r  = col.r;
	innerLight.Ambient.g  = col.g;
	innerLight.Ambient.b  = col.b;
	innerLight.Specular.r = 1.0f;
	innerLight.Specular.g = 1.0f;
	innerLight.Specular.b = 1.0f;
	innerLight.Position.x = 0.0f;
	innerLight.Position.y = 0.0f;
	innerLight.Position.z = 0.0f;
	innerLight.Range = 1.0f;
	innerLight.Attenuation0 = 0.0f;
	innerLight.Attenuation1 = 1.5f;
	innerLight.Attenuation2 = 0.0f;
	indexLight = -1;
}

/************************************************************************/

Ghost::~Ghost()
{
	if (ghtDatas) { delete[] ghtDatas;}
	if (ghtVertices) {
		delete[] ghtVertices;
	}
	if (vBGht) {
		vBGht->Release();
		vBGht = NULL;
	}
}

/************************************************************************/

ghtType Ghost::GetGhtTypeFromPath(char* path)
{
	ghtType type;
	char* ptr = path;
	int i = 0;
	while ( *(ptr+i) != '.') {
		i++;
	}
	if (*(ptr+i+1) == 's') {
		// Fichier .seq
		type = SEQ;
	} else {
		// Fichier .ght
		type = GHT;
	}
	return type;
}

/************************************************************************/

bool Ghost::LoadGht(char* path)
{
	long fileSize;
	ghtType type = GetGhtTypeFromPath(path);

	if (fopen_s(&ghtFile, path, "rb"))
		return false;

	fseek(ghtFile, 0, SEEK_END);
	fileSize = ftell(ghtFile);
	numGhtPoints = (fileSize - 0x158) / 64;

	ReadHeader(type == GHT ? 0x88 : 0x6C);

	ghtDatas = new GHTPOINT[numGhtPoints];
	for (int i = 0; i < numGhtPoints; i++)
	{
		int dw;
		fread(&dw, 4, 1, ghtFile);								// Temps
		ghtDatas[i].time = FIX2MS(dw);
		fread(&dw, 4, 1, ghtFile);								// Révolution roues
		fread(&dw, 4, 1, ghtFile);								// Braquage roues
		fread(&dw, 4, 1, ghtFile);								// Suspension roue 1
		fread(&dw, 4, 1, ghtFile);								// Suspension roue 2
		fread(&dw, 4, 1, ghtFile);								// Suspension roue 3
		fread(&dw, 4, 1, ghtFile);								// Suspension roue 4
		fread(&dw, 4, 1, ghtFile);								// Centre de gravité : coordonnée x
		ghtDatas[i].gPoint.x = ((float)dw) / 6553600;
		fread(&dw, 4, 1, ghtFile);								// Centre de gravité : coordonnée y
		ghtDatas[i].gPoint.y = ((float)dw) / 6553600;
		fread(&dw, 4, 1, ghtFile);								// Centre de gravité : coordonnée z
		ghtDatas[i].gPoint.z = ((float)dw) / 6553600;
		fread(&dw, 4, 1, ghtFile);
		ghtDatas[i].xAxis.x = ((float)dw) / 65536;			// Axe X : coordonnée x
		fread(&dw, 4, 1, ghtFile);
		ghtDatas[i].xAxis.y = ((float)dw) / 65536;			// Axe X : coordonnée y
		fread(&dw, 4, 1, ghtFile);
		ghtDatas[i].xAxis.z = ((float)dw) / 65536;			// Axe X : coordonnée z
		fread(&dw, 4, 1, ghtFile);
		ghtDatas[i].yAxis.x = ((float)dw) / 65536;			// Axe Y : coordonnée x
		fread(&dw, 4, 1, ghtFile);
		ghtDatas[i].yAxis.y = ((float)dw) / 65536;			// Axe Y : coordonnée y
		fread(&dw, 4, 1, ghtFile);
		ghtDatas[i].yAxis.z = ((float)dw) / 65536;			// Axe Y : coordonnée z
	}

	fclose(ghtFile);
	return true;
}

/************************************************************************/

void Ghost::Init(IDirect3DDevice9 *dev, int index)
{
	int i;

	indexLight = index;

	// Création du mesh
	numGhtVertices = 60;
	ghtVertices = new VERTEXBUF[numGhtVertices];

		// Pas de primitive indexées pour le moment car ceci est temporaire. Dans une version future, le mesh de la voiture (codé ici sous forme simpliste) sera lu à partir du fichier bv4
	ghtVertices[0].x=-3.565f;  ghtVertices[0].y= 2.058f;  ghtVertices[0].z=-1.095f;  ghtVertices[0].color=color+0x10000000;  ghtVertices[0].u=0.0f;  ghtVertices[0].v=0.0f;
	ghtVertices[1].x= 4.966f;  ghtVertices[1].y= 2.058f;  ghtVertices[1].z=-1.095f;  ghtVertices[1].color=color+0x10000000;  ghtVertices[1].u=1.0f;  ghtVertices[1].v=0.0f;
	ghtVertices[2].x= 4.966f;  ghtVertices[2].y= -1.98f;  ghtVertices[2].z=-1.095f;  ghtVertices[2].color=color+0x10000000;  ghtVertices[2].u=0.5f;  ghtVertices[2].v=1.0f;
	ghtVertices[3].x=-3.565f;  ghtVertices[3].y= 2.058f;  ghtVertices[3].z=-1.095f;  ghtVertices[3].color=color+0x10000000;  ghtVertices[3].u=0.0f;  ghtVertices[3].v=0.0f;
	ghtVertices[4].x= 4.966f;  ghtVertices[4].y= -1.98f;  ghtVertices[4].z=-1.095f;  ghtVertices[4].color=color+0x10000000;  ghtVertices[4].u=1.0f;  ghtVertices[4].v=0.0f;
	ghtVertices[5].x=-3.565f;  ghtVertices[5].y= -1.98f;  ghtVertices[5].z=-1.095f;  ghtVertices[5].color=color+0x10000000;  ghtVertices[5].u=0.5f;  ghtVertices[5].v=1.0f;

	ghtVertices[12].x=-3.565f;  ghtVertices[12].y= -1.98f;  ghtVertices[12].z=-1.095f;  ghtVertices[12].color=color+0x10000000;  ghtVertices[12].u=0.0f;  ghtVertices[12].v=0.0f;
	ghtVertices[13].x= 4.966f;  ghtVertices[13].y= -1.98f;  ghtVertices[13].z=-1.095f;  ghtVertices[13].color=color+0x10000000;  ghtVertices[13].u=1.0f;  ghtVertices[13].v=0.0f;
	ghtVertices[14].x= 4.966f;  ghtVertices[14].y= -1.98f;  ghtVertices[14].z= 0.0f;	ghtVertices[14].color=color+0x10000000;  ghtVertices[14].u=0.5f;  ghtVertices[14].v=1.0f;
	ghtVertices[15].x=-3.565f;  ghtVertices[15].y= -1.98f;  ghtVertices[15].z=-1.095f;  ghtVertices[15].color=color+0x10000000;  ghtVertices[15].u=0.0f;  ghtVertices[15].v=0.0f;
	ghtVertices[16].x= 4.966f;  ghtVertices[16].y= -1.98f;  ghtVertices[16].z=0.0f;		ghtVertices[16].color=color+0x10000000;  ghtVertices[16].u=1.0f;  ghtVertices[16].v=0.0f;
	ghtVertices[17].x=-3.565f;  ghtVertices[17].y= -1.98f;  ghtVertices[17].z= 0.0f;	ghtVertices[17].color=color+0x10000000;  ghtVertices[17].u=0.5f;  ghtVertices[17].v=1.0f;

	ghtVertices[18].x=-3.565f;  ghtVertices[18].y= 2.058f;  ghtVertices[18].z=-1.095f;  ghtVertices[18].color=color+0x10000000;  ghtVertices[18].u=0.0f;  ghtVertices[18].v=0.0f;
	ghtVertices[19].x= 4.966f;  ghtVertices[19].y= 2.058f;  ghtVertices[19].z= 0.0f;	ghtVertices[19].color=color+0x10000000;  ghtVertices[19].u=0.5f;  ghtVertices[19].v=1.0f;
	ghtVertices[20].x= 4.966f;  ghtVertices[20].y= 2.058f;  ghtVertices[20].z=-1.095f;  ghtVertices[20].color=color+0x10000000;  ghtVertices[20].u=1.0f;  ghtVertices[20].v=0.0f;
	ghtVertices[21].x=-3.565f;  ghtVertices[21].y= 2.058f;  ghtVertices[21].z=-1.095f;  ghtVertices[21].color=color+0x10000000;  ghtVertices[21].u=0.0f;  ghtVertices[21].v=0.0f;
	ghtVertices[22].x=-3.565f;  ghtVertices[22].y= 2.058f;  ghtVertices[22].z= 0.0f;	ghtVertices[22].color=color+0x10000000;  ghtVertices[22].u=0.5f;  ghtVertices[22].v=1.0f;
	ghtVertices[23].x= 4.966f;  ghtVertices[23].y= 2.058f;  ghtVertices[23].z=0.0f;		ghtVertices[23].color=color+0x10000000;  ghtVertices[23].u=1.0f;  ghtVertices[23].v=0.0f;

	ghtVertices[24].x=-3.565f;  ghtVertices[24].y= 2.058f;  ghtVertices[24].z=-1.095f;  ghtVertices[24].color=color+0x10000000;  ghtVertices[24].u=1.0f;  ghtVertices[24].v=0.0f;
	ghtVertices[25].x=-3.565f;  ghtVertices[25].y= -1.98f;  ghtVertices[25].z=-1.095f;  ghtVertices[25].color=color+0x10000000;  ghtVertices[25].u=1.0f;  ghtVertices[25].v=0.0f;
	ghtVertices[26].x=-3.565f;  ghtVertices[26].y= -1.98f;  ghtVertices[26].z= 0.0f;	ghtVertices[26].color=color+0x10000000;  ghtVertices[26].u=1.0f;  ghtVertices[26].v=0.0f;
	ghtVertices[27].x=-3.565f;  ghtVertices[27].y= 2.058f;  ghtVertices[27].z=-1.095f;  ghtVertices[27].color=color+0x10000000;  ghtVertices[27].u=1.0f;  ghtVertices[27].v=0.0f;
	ghtVertices[28].x=-3.565f;  ghtVertices[28].y= -1.98f;  ghtVertices[28].z= 0.0f;	ghtVertices[28].color=color+0x10000000;  ghtVertices[28].u=1.0f;  ghtVertices[28].v=0.0f;
	ghtVertices[29].x=-3.565f;  ghtVertices[29].y= 2.058f;  ghtVertices[29].z= 0.0f;	ghtVertices[29].color=color+0x10000000;  ghtVertices[29].u=1.0f;  ghtVertices[29].v=0.0f;

	ghtVertices[30].x=4.966f;  ghtVertices[30].y= 2.058f;  ghtVertices[30].z=-1.095f;	ghtVertices[30].color=color+0x10000000;  ghtVertices[30].u=1.0f;  ghtVertices[30].v=0.0f;
	ghtVertices[31].x=4.966f;  ghtVertices[31].y= -1.98f;  ghtVertices[31].z= 0.0f;		ghtVertices[31].color=color+0x10000000;  ghtVertices[31].u=1.0f;  ghtVertices[31].v=0.0f;
	ghtVertices[32].x=4.966f;  ghtVertices[32].y= -1.98f;  ghtVertices[32].z=-1.095f;	ghtVertices[32].color=color+0x10000000;  ghtVertices[32].u=1.0f;  ghtVertices[32].v=0.0f;
	ghtVertices[33].x=4.966f;  ghtVertices[33].y= 2.058f;  ghtVertices[33].z=-1.095f;	ghtVertices[33].color=color+0x10000000;  ghtVertices[33].u=1.0f;  ghtVertices[33].v=0.0f;
	ghtVertices[34].x=4.966f;  ghtVertices[34].y= 2.058f;  ghtVertices[34].z= 0.0f;		ghtVertices[34].color=color+0x10000000;  ghtVertices[34].u=1.0f;  ghtVertices[34].v=0.0f;
	ghtVertices[35].x=4.966f;  ghtVertices[35].y= -1.98f;  ghtVertices[35].z= 0.0f;		ghtVertices[35].color=color+0x10000000;  ghtVertices[35].u=1.0f;  ghtVertices[35].v=0.0f;

	ghtVertices[6].x=-3.565f;  ghtVertices[6].y= 2.058f;  ghtVertices[6].z=1.5f;		ghtVertices[6].color=color;  ghtVertices[6].u=0.0f;  ghtVertices[6].v=0.0f;
	ghtVertices[7].x= 2.773f;  ghtVertices[7].y= -1.98f;  ghtVertices[7].z=1.5f;		ghtVertices[7].color=color;  ghtVertices[7].u=0.5f;  ghtVertices[7].v=1.0f;
	ghtVertices[8].x= 2.773f;  ghtVertices[8].y= 2.058f;  ghtVertices[8].z=1.5f;		ghtVertices[8].color=color;  ghtVertices[8].u=1.0f;  ghtVertices[8].v=0.0f;
	ghtVertices[9].x=-3.565f;  ghtVertices[9].y= 2.058f;  ghtVertices[9].z=1.5f;		ghtVertices[9].color=color;  ghtVertices[9].u=0.0f;  ghtVertices[9].v=0.0f;
	ghtVertices[10].x=-3.565f;  ghtVertices[10].y= -1.98f;  ghtVertices[10].z=1.5f;		ghtVertices[10].color=color;  ghtVertices[10].u=0.5f;  ghtVertices[10].v=1.0f;
	ghtVertices[11].x= 2.773f;  ghtVertices[11].y= -1.98f;  ghtVertices[11].z=1.5f;		ghtVertices[11].color=color;  ghtVertices[11].u=1.0f;  ghtVertices[11].v=0.0f;

	ghtVertices[36].x=-3.565f;  ghtVertices[36].y= -1.98f;  ghtVertices[36].z=0.0f;		ghtVertices[36].color=color+0x10000000;  ghtVertices[36].u=0.0f;  ghtVertices[36].v=0.0f;
	ghtVertices[37].x= 4.966f;  ghtVertices[37].y= -1.98f;  ghtVertices[37].z=0.0f;		ghtVertices[37].color=color+0x10000000;  ghtVertices[37].u=1.0f;  ghtVertices[37].v=0.0f;
	ghtVertices[38].x= 2.773f;  ghtVertices[38].y= -1.98f;  ghtVertices[38].z= 1.5f;	ghtVertices[38].color=color+0x10000000;  ghtVertices[38].u=0.5f;  ghtVertices[38].v=1.0f;
	ghtVertices[39].x=-3.565f;  ghtVertices[39].y= -1.98f;  ghtVertices[39].z=0.0f;		ghtVertices[39].color=color+0x10000000;  ghtVertices[39].u=0.0f;  ghtVertices[39].v=0.0f;
	ghtVertices[40].x= 2.773f;  ghtVertices[40].y= -1.98f;  ghtVertices[40].z=1.5f;		ghtVertices[40].color=color+0x10000000;  ghtVertices[40].u=1.0f;  ghtVertices[40].v=0.0f;
	ghtVertices[41].x=-3.565f;  ghtVertices[41].y= -1.98f;  ghtVertices[41].z= 1.5f;	ghtVertices[41].color=color+0x10000000;  ghtVertices[41].u=0.5f;  ghtVertices[41].v=1.0f;

	ghtVertices[42].x=-3.565f;  ghtVertices[42].y= 2.058f;  ghtVertices[42].z=0.0f;		ghtVertices[42].color=color+0x10000000;  ghtVertices[42].u=0.0f;  ghtVertices[42].v=0.0f;
	ghtVertices[43].x= 2.773f;  ghtVertices[43].y= 2.058f;  ghtVertices[43].z= 1.5f;	ghtVertices[43].color=color+0x10000000;  ghtVertices[43].u=0.5f;  ghtVertices[43].v=1.0f;
	ghtVertices[44].x= 4.966f;  ghtVertices[44].y= 2.058f;  ghtVertices[44].z=0.0f;		ghtVertices[44].color=color+0x10000000;  ghtVertices[44].u=1.0f;  ghtVertices[44].v=0.0f;
	ghtVertices[45].x=-3.565f;  ghtVertices[45].y= 2.058f;  ghtVertices[45].z=0.0f;		ghtVertices[45].color=color+0x10000000;  ghtVertices[45].u=0.0f;  ghtVertices[45].v=0.0f;
	ghtVertices[46].x=-3.565f;  ghtVertices[46].y= 2.058f;  ghtVertices[46].z= 1.5f;	ghtVertices[46].color=color+0x10000000;  ghtVertices[46].u=0.5f;  ghtVertices[46].v=1.0f;
	ghtVertices[47].x= 2.773f;  ghtVertices[47].y= 2.058f;  ghtVertices[47].z=1.5f;		ghtVertices[47].color=color+0x10000000;  ghtVertices[47].u=1.0f;  ghtVertices[47].v=0.0f;

	ghtVertices[48].x=-3.565f;  ghtVertices[48].y= 2.058f;  ghtVertices[48].z=0.0f;		ghtVertices[48].color=color+0x10000000;  ghtVertices[48].u=1.0f;  ghtVertices[48].v=0.0f;
	ghtVertices[49].x=-3.565f;  ghtVertices[49].y= -1.98f;  ghtVertices[49].z=0.0f;		ghtVertices[49].color=color+0x10000000;  ghtVertices[49].u=1.0f;  ghtVertices[49].v=0.0f;
	ghtVertices[50].x=-3.565f;  ghtVertices[50].y= -1.98f;  ghtVertices[50].z= 1.5f;	ghtVertices[50].color=color+0x10000000;  ghtVertices[50].u=1.0f;  ghtVertices[50].v=0.0f;
	ghtVertices[51].x=-3.565f;  ghtVertices[51].y= 2.058f;  ghtVertices[51].z=0.0f;		ghtVertices[51].color=color+0x10000000;  ghtVertices[51].u=1.0f;  ghtVertices[51].v=0.0f;
	ghtVertices[52].x=-3.565f;  ghtVertices[52].y= -1.98f;  ghtVertices[52].z= 1.5f;	ghtVertices[52].color=color+0x10000000;  ghtVertices[52].u=1.0f;  ghtVertices[52].v=0.0f;
	ghtVertices[53].x=-3.565f;  ghtVertices[53].y= 2.058f;  ghtVertices[53].z= 1.5f;	ghtVertices[53].color=color+0x10000000;  ghtVertices[53].u=1.0f;  ghtVertices[53].v=0.0f;

	ghtVertices[54].x=4.966f;  ghtVertices[54].y= 2.058f;  ghtVertices[54].z=0.0f;		ghtVertices[54].color=color+0x10000000;  ghtVertices[54].u=1.0f;  ghtVertices[54].v=0.0f;
	ghtVertices[55].x=2.773f;  ghtVertices[55].y= -1.98f;  ghtVertices[55].z= 1.5f;		ghtVertices[55].color=color+0x10000000;  ghtVertices[55].u=1.0f;  ghtVertices[55].v=0.0f;
	ghtVertices[56].x=4.966f;  ghtVertices[56].y= -1.98f;  ghtVertices[56].z=0.0f;		ghtVertices[56].color=color+0x10000000;  ghtVertices[56].u=1.0f;  ghtVertices[56].v=0.0f;
	ghtVertices[57].x=4.966f;  ghtVertices[57].y= 2.058f;  ghtVertices[57].z=0.0f;		ghtVertices[57].color=color+0x10000000;  ghtVertices[57].u=1.0f;  ghtVertices[57].v=0.0f;
	ghtVertices[58].x=2.773f;  ghtVertices[58].y= 2.058f;  ghtVertices[58].z= 1.5f;		ghtVertices[58].color=color+0x10000000;  ghtVertices[58].u=1.0f;  ghtVertices[58].v=0.0f;
	ghtVertices[59].x=2.773f;  ghtVertices[59].y= -1.98f;  ghtVertices[59].z= 1.5f;		ghtVertices[59].color=color+0x10000000;  ghtVertices[59].u=1.0f;  ghtVertices[59].v=0.0f;

	dev->CreateVertexBuffer(sizeof(VERTEXBUF) * numGhtVertices,
	D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0),
	D3DPOOL_DEFAULT,
	&vBGht,
	NULL);

	vBGht->Lock(0, 0, (void**)&v, 0);
	for(i=0; i<numGhtVertices; i++)
	{
		// Echelle D3D
		ghtVertices[i].x = ghtVertices[i].x / 100;
		ghtVertices[i].y = ghtVertices[i].y / 100;
		ghtVertices[i].z = ghtVertices[i].z / 100 + 0.011f;

		v[i].x = ghtVertices[i].x;
		v[i].y = ghtVertices[i].y;
		v[i].z = ghtVertices[i].z;
		v[i].color = ghtVertices[i].color;
		v[i].u = ghtVertices[i].u;
		v[i].v = ghtVertices[i].v;
	}
	vBGht->Unlock();

	// Renderstates spécifiques aux ghosts : alphablending et lumière
	dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	dev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	dev->SetRenderState(D3DRS_LIGHTING, TRUE);

	// Lumière
	dev->SetLight(indexLight, &innerLight);
	dev->LightEnable(indexLight, lightOn);

	// traces
	if (tracesOn) InitTraces();
}

/************************************************************************/

void Ghost::Rewind()
{
	currentGhtPoint = 0;
}

/************************************************************************/

void Ghost::Update(IDirect3DDevice9 *dev, long time)
{
	// Recherche la valeur currentGhtPoint telle que
	// ghtDatas[currentGhtPoint].time  <=  time  <  ghtDatas[currentGhtPoint+1].time
	if ( time > ghtDatas[currentGhtPoint].time ) {
		bool goOn = true;
		while ( (currentGhtPoint+1 < numGhtPoints) && goOn) {
			if ( time >= ghtDatas[currentGhtPoint+1].time) {
				currentGhtPoint++;
			} else {
				goOn = false;
			}
		}
	}

	// Calcul du rapport de proportionalité
	float ratio = ((float) (time - ghtDatas[currentGhtPoint].time)) / ((float) (ghtDatas[currentGhtPoint+1].time - ghtDatas[currentGhtPoint].time));
	POINT3D r{}, x{}, y{};

	// Interpolation
	if (currentGhtPoint + 1 < numGhtPoints)
	{
		r.x = ratio * (ghtDatas[currentGhtPoint + 1].gPoint.x - ghtDatas[currentGhtPoint].gPoint.x);
		r.y = ratio * (ghtDatas[currentGhtPoint + 1].gPoint.y - ghtDatas[currentGhtPoint].gPoint.y);
		r.z = ratio * (ghtDatas[currentGhtPoint + 1].gPoint.z - ghtDatas[currentGhtPoint].gPoint.z);

		x.x = ratio * (ghtDatas[currentGhtPoint + 1].xAxis.x - ghtDatas[currentGhtPoint].xAxis.x);
		x.y = ratio * (ghtDatas[currentGhtPoint + 1].xAxis.y - ghtDatas[currentGhtPoint].xAxis.y);
		x.z = ratio * (ghtDatas[currentGhtPoint + 1].xAxis.z - ghtDatas[currentGhtPoint].xAxis.z);

		y.x = ratio * (ghtDatas[currentGhtPoint + 1].yAxis.x - ghtDatas[currentGhtPoint].yAxis.x);
		y.y = ratio * (ghtDatas[currentGhtPoint + 1].yAxis.y - ghtDatas[currentGhtPoint].yAxis.y);
		y.z = ratio * (ghtDatas[currentGhtPoint + 1].yAxis.z - ghtDatas[currentGhtPoint].yAxis.z);
	}

	ghtPoint.time = time;
	ghtPoint.gPoint = {
		r.x + ghtDatas[currentGhtPoint].gPoint.x,
		r.y + ghtDatas[currentGhtPoint].gPoint.y,
		r.z + ghtDatas[currentGhtPoint].gPoint.z
	};
	ghtPoint.xAxis = {
		x.x + ghtDatas[currentGhtPoint].xAxis.x,
		x.y + ghtDatas[currentGhtPoint].xAxis.y,
		x.z + ghtDatas[currentGhtPoint].xAxis.z
	};
	ghtPoint.yAxis = {
		y.x + ghtDatas[currentGhtPoint].yAxis.x,
		y.y + ghtDatas[currentGhtPoint].yAxis.y,
		y.z + ghtDatas[currentGhtPoint].yAxis.z
	};

	// MAJ de la position interpolée de la source lumineuse
	if (lightOn)
	{
		innerLight.Position.x = ghtPoint.gPoint.x;
		innerLight.Position.y = ghtPoint.gPoint.y;
		innerLight.Position.z = -ghtPoint.gPoint.z;
		dev->SetLight(indexLight, &innerLight);
	}
}

/************************************************************************/

void Ghost::Draw(IDirect3DDevice9* dev)
{
	// Calculate transformation matrix.
	D3DXMATRIX world, prevWorld;
	CalculateWorld(&world, &ghtPoint.gPoint, &ghtPoint.xAxis, &ghtPoint.yAxis);
	dev->GetTransform(D3DTS_WORLD, &prevWorld);
	D3DXMatrixMultiply(&world, &world, &prevWorld);

	// Affichage voiture
	dev->SetTransform(D3DTS_WORLD, &world);
	dev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	dev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	dev->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0));
	dev->SetVertexShader(NULL);
	dev->SetTexture(0, NULL); // TODO: Possibly no longer needed if mirror is implemented
	dev->SetStreamSource(0, vBGht, 0, sizeof(VERTEXBUF));
	dev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, numGhtVertices / 3);
	dev->SetTransform(D3DTS_WORLD, &prevWorld);
	
	// Affichage traces
	if (tracesOn) DrawTraces(dev);
	dev->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

/************************************************************************/

void Ghost::ReadHeader(int offset)
{
	int dw;

	fseek(ghtFile, offset, SEEK_SET);
	fread(&dw, 4, 1, ghtFile);
	raceStartTime = FIX2MS(dw);

	for (int l = 0; l < 3; l++)
	{
		fread(&dw, 4, 1, ghtFile);
		lapTimes[l] = FIX2MS(dw);
	}
	for (int l = 0; l < 3; l++)
	{
		for (int p = 0; p < 4; p++)
		{
			fread(&dw, 4, 1, ghtFile);
			partTimes[l][p] = FIX2MS(dw);
		}
	}

	fseek(ghtFile, offset + 0x58, SEEK_SET);
	fread(vehicleName, 1, 8, ghtFile);

	fseek(ghtFile, offset + 0x7C, SEEK_SET);
	fread(pilotName, 1, 8, ghtFile);

	fseek(ghtFile, offset + 0xD0, SEEK_SET);
}

/************************************************************************/

D3DXMATRIX* Ghost::CalculateWorld(D3DXMATRIX* world, POINT3D* pos, POINT3D* xAxis, POINT3D* yAxis)
{
	memset(world, 0, sizeof(D3DXMATRIX));

	D3DXVECTOR3 eye(0.0f, 0.0f, xAxis->x * yAxis->y - yAxis->x * xAxis->y);
	D3DXVECTOR3 up(xAxis->y, yAxis->y, xAxis->z * yAxis->x - xAxis->x * yAxis->z);
	D3DXVECTOR3 at(xAxis->z, yAxis->z, eye.z);
	eye.z = 0.0f;

	D3DXMatrixLookAtLH(world, &eye, &at, &up);

	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, pos->x, pos->y, pos->z);
	D3DXMatrixMultiply(world, world, &trans);

	D3DXPLANE plane;
	D3DXVECTOR3 a(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 b(1.0f, 0.0f, 0.0f);
	D3DXVECTOR3 c(0.0f, 1.0f, 0.0f);
	D3DXPlaneFromPoints(&plane, &a, &b, &c);
	D3DXMatrixReflect(&trans, &plane);
	D3DXMatrixMultiply(world, world, &trans);

	return world;
}

/************************************************************************/

void Ghost::InitTraces()
{
	if (numTraces > 0 ) {
		subAlphaTrace = ((color >> 24) / numTraces ) << 24;
	}
}

/************************************************************************/

void Ghost::DrawTraces(IDirect3DDevice9 *dev)
{
	int currentTrace = 0, currentTraceGhtPoint = currentGhtPoint - currentTrace;
	unsigned int subTraceColor;
	D3DXMATRIX world, prevWorld;
	dev->GetTransform(D3DTS_WORLD, &prevWorld);

	while ( (currentTrace <= numTraces) && (currentTraceGhtPoint >= 0)) {
		// Set transparent colors.
		subTraceColor = subAlphaTrace * currentTrace;
		vBGht->Lock(0, 0, (void**)&v, 0);
		for(int i=0; i<numGhtVertices; i++)
			v[i].color = ghtVertices[i].color - subTraceColor;
		vBGht->Unlock();

		// Set world matrix.
		CalculateWorld(&world,
			&ghtDatas[currentTraceGhtPoint].gPoint,
			&ghtDatas[currentTraceGhtPoint].xAxis,
			&ghtDatas[currentTraceGhtPoint].yAxis);
		D3DXMATRIX scale;
		D3DXMatrixScaling(&scale, 1.0f, 0.96f, 0.96f);
		D3DXMatrixMultiply(&world, &scale, &world);
		D3DXMatrixMultiply(&world, &world, &prevWorld);
		dev->SetTransform(D3DTS_WORLD, &world);

		dev->SetStreamSource(0, vBGht, 0, sizeof(VERTEXBUF));
		dev->DrawPrimitive(D3DPT_TRIANGLELIST, 0, numGhtVertices / 3);

		currentTrace++;
		currentTraceGhtPoint = currentGhtPoint - currentTrace;
	}

	// Restore world matrix.
	dev->SetTransform(D3DTS_WORLD, &prevWorld);
	
	// Restore opaque colors.
	vBGht->Lock(0, 0, (void**)&v, 0);
	for (int i = 0; i < numGhtVertices; i++)
		v[i].color = ghtVertices[i].color;
	vBGht->Unlock();
}

/************************************************************************/

void Ghost::EnableTraces( bool val)
{
	tracesOn = val;
	if (tracesOn) InitTraces();
}

/************************************************************************/

void Ghost::EnableInnerLight(IDirect3DDevice9 *dev, bool val)
{
	if (indexLight >= 0) {
		lightOn = val;
		dev->LightEnable(indexLight, lightOn);
	}
}
