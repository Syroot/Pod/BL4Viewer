//**************************************************************************
//* ghost.h	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Gére un ghost
//***************************************************************************


#ifndef __GHOST_H
#define __GHOST_H

#include <d3d9.h>
#include <d3dx9.h>
#include "stdio.h"
#include "struct3d.h"

enum ghtType { GHT, SEQ };

struct GHTPOINT
{
	long time;				// Temps en ms
	POINT3D gPoint;			// Centre de gravité
	POINT3D xAxis;			// Axe X
	POINT3D yAxis;			// Axe Y
};

class Ghost {
public:
	Ghost(D3DCOLORVALUE col, int nTraces);				// Constructeur prend en paramètre les composantes couleurs du ghost et le nombre de traces
	~Ghost();

	inline unsigned int const GetColor() const		{ return color; }	// Accesseurs
	inline const char* GetVehicleName() const		{ return vehicleName; }
	inline const char* GetPilotName() const			{ return pilotName; }
	inline int GetRaceStartTime() const				{ return raceStartTime;}
	inline int GetLapTime(int lap) const			{ return lapTimes[lap]; }
	inline int GetPartTime(int lap, int part) const	{ return partTimes[lap][part]; }
	inline void SetNumTraces( int val)				{ numTraces = val; }
	inline bool IsTracesOn() const					{ return tracesOn; }
	inline bool IsLightOn() const					{ return lightOn; }
	inline const GHTPOINT* GetGhtPoint() const		{ return &ghtPoint; }

	bool LoadGht( char* path);								// Charge un fichier seq ou ght
	void Init(IDirect3DDevice9 *dev, int index);			// Initialisations ( vertexBuffers , lumière, renderstates). L'index en paramètre est le numéro d'assignation de la lumière du ghost au device.
	void Rewind();											// Réinitialisation de la position courante du ghost (currentGhtPoint)
	void Update(IDirect3DDevice9 *dev, long time);			// Update ghost transforms
	void Draw(IDirect3DDevice9* dev);						// Affichage du ghost au temps time
	void EnableTraces( bool val);							// Active/desactive les traces
	void EnableInnerLight(IDirect3DDevice9 *dev, bool val);	// Active/désactive la lumière

private:
	void ReadHeader(int offset);				// Reads the header from the given offset
	D3DXMATRIX* CalculateWorld(D3DXMATRIX* world, POINT3D* pos, POINT3D* xAxis, POINT3D* yAxis); // Calculates the ghost transformation matrix
	void InitTraces();							// Pré-calculs nécessaires à l'affichage des traces
	void DrawTraces(IDirect3DDevice9 *dev);		// Affichage des traces ( cad les numTraces positions précédentes avec une valeur alpha décroissante)
	ghtType GetGhtTypeFromPath(char* path);		// Retourne le type de fichier (ght ou seq) dont le chemin est passé en paramètre

	FILE* ghtFile;						// Fichier ght
	GHTPOINT* ghtDatas;					// Données du tracé du ght
	long numGhtPoints;					// Nombre de données du tracé
	int currentGhtPoint;				// Indice de la position courante (par défaut) dans le tableau de données du tracé
	GHTPOINT ghtPoint;
	long raceStartTime;					// Temps au bout duquel la voiture franchit la ligne de départ pour la 1ère fois (début course)
	long lapTimes[3];					// Lap completion times, relative to raceStartTime
	long partTimes[3][4];				// Part times for each lap (0 if part is not used)
	IDirect3DVertexBuffer9 *vBGht;		// Voiture
	int numGhtVertices;					// Nombre de sommets
	VERTEXBUF *v;						// Utilisé pour accéder au VB
	VERTEXBUF *ghtVertices;				// Sommets du mesh au chargement
	unsigned int color;					// Couleur du ghost
	char vehicleName[8];				// Name of car
	char pilotName[8];					// Nom du pilote
	bool tracesOn;						// Affichage des traces
	int numTraces;						// Nombre de traces
	unsigned int subAlphaTrace;			// Valeur à soustraire à color pour obtenir la couleur d'une trace
	bool lightOn;						// Affichage de la lumière
	D3DLIGHT9 innerLight;				// Lumière Point Light à la position de la voiture
	int indexLight;						// Index d'assignation de la lumière au device (cf Init())
};

#endif // __GHOST_H
