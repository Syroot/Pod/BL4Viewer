//**************************************************************************
//* misc.cpp	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Ce module contient des méthodes diverses
//***************************************************************************

#include "misc.h"

char* OpenSaveFile(const HWND hHandle, const bool bSave,	const char *FileTypes, const char *DefExt)
{

// Ouvrir la boîte de dialogue "Ouvrir..." ou "Enregistrer sous..."

char* path = NULL;
OPENFILENAME file;
// Va contenir le nom du fichier à ouvrir ou à enregistrer
char FileName[MAX_PATH];

	  // Réserve de la mémoire pour la struct
	  ZeroMemory(&file, sizeof(file));
	  // Initialise à zéro
	  FileName[0] = 0;

	  // Vérifie si un type et l'ext. par defaut à été indiquer
	  if (!strlen(FileTypes) || !strlen(DefExt))
	  { // Sinon on l'indique
			file.lpstrFilter = "All Files (*.*)\0*.*\0\0";
			file.lpstrDefExt = "*.*";
	  }
	  else
	  { // Ceux en paramètres
			file.lpstrFilter = FileTypes;
			file.lpstrDefExt = DefExt;
	  }

	  // Taille de la structure
	  file.lStructSize = sizeof(file);
	  // Handle de la fenêtre appelante
	  file.hwndOwner = hHandle;
	  // Nom du fichier
	  file.lpstrFile = FileName;
	  // Longeur maximum
	  file.nMaxFile = MAX_PATH;

	  // Enregistrer sous...
	  if (bSave == true)
	  {
			// Options
			file.Flags = OFN_EXPLORER|OFN_PATHMUSTEXIST|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT;
			// S'il n'y à pas d'erreur
			if (GetSaveFileName(&file))
			{
				path = FileName;
			}
	  }
	  else  // Ouvrir...
	  {
			// Options
			file.Flags = OFN_EXPLORER|OFN_FILEMUSTEXIST|OFN_HIDEREADONLY;
			// Ouvrir...
			if (GetOpenFileName(&file))
			{
				path = FileName;
			}
	  }
	  // Renvoie le résultat
	  if (path != NULL) {
		char* path2 = (char*) malloc(strlen(path)+1);
		strcpy_s(path2, strlen(path) + 1, path);
		return path2;
	  } else {
		  return path;
	  }
}

//***************************************************************************

char* ExtractNameFromPath( char* path)
{
	char* name = NULL;
	if (path != NULL) {
		char* ptr = path;
		int i = 0, last = 0, length;
		while ( *(ptr+i) != '.') {
			if ( *(ptr+i) == '\\') { last = i; }
			i++;
		}
		length = i - last;
		name = (char*) malloc(length);
		int j = 0;
		for (i=last+1; i<last+length; i++) {
			*(name+j) = *(ptr+i);
			j++;
		}
		*(name+j) = '\0';
	}
	return name;
}

//***************************************************************************

BinMemFile::BinMemFile(long size)
{
	buffer = new BYTE[size];
	current = buffer;
}

//***************************************************************************

BinMemFile::~BinMemFile()
{
	current = NULL;
	delete[] buffer;
	buffer = NULL;
}

//***************************************************************************

void BinMemFile::fread(void* buf, binMemFileMode mode, int num)
{
	BYTE* bBufPtr;
	int* iPtr, *iBufPtr;
	int i;

	switch (mode) {
		case (BMF_BYTE) :
			bBufPtr = (BYTE*) buf;
			for (i=0; i<num; i++) {
				*(bBufPtr+i) = *(current+i);
			}
			current += num;
			break;
		case (BMF_DWORD) :
			iPtr = (int*) current;
			iBufPtr = (int*) buf;
			for (i=0; i<num; i++) {
				*(iBufPtr+i) = *(iPtr+i);			// *4 à vérifier
			}
			current = (BYTE*) (iPtr+num);
			break;
	}
}

//***************************************************************************

void BinMemFile::fwrite(const void* buf, binMemFileMode mode, int num)
{
	BYTE* bBufPtr;
	int* iPtr, *iBufPtr;
	int i;

	switch (mode) {
		case (BMF_BYTE) :
			bBufPtr = (BYTE*) buf;
			for (i=0; i<num; i++) {
				*(current+i) = *(bBufPtr+i);
			}
			current += num;
			break;
		case (BMF_DWORD) :
			iPtr = (int*) current;
			iBufPtr = (int*) buf;
			for (i=0; i<num; i++) {
				*(iPtr+i) = *(iBufPtr+i);			// *4 à vérifier
			}
			current = (BYTE*) (iPtr+num);
			break;
	}
}

//***************************************************************************

void BinMemFile::rewind()
{
	current = buffer;
}

//***************************************************************************

void BinMemFile::fseek(long offset)
{
	current = buffer + offset;
}
