//**************************************************************************
//* misc.h	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Ce module contient des méthodes diverses
//***************************************************************************

#ifndef __MISC_H
#define __MISC_H

#include <windows.h>
#include <malloc.h>
#include <commdlg.h>

// Crée une boîte de dialogue pour ouvrir ou sauver un fichier
char* OpenSaveFile(const HWND hHandle, const bool bSave,	const char *FileTypes, const char *DefExt);

// Retourne le nom du fichier (sans l'extension) dont le chemin est passé en paramètre
char* ExtractNameFromPath( char* path);



//***************************************************************************

// Pour lire et écrire un fichier binaire en mémoire
enum binMemFileMode { BMF_BYTE, BMF_DWORD };

class BinMemFile {
private :
	BYTE* buffer;	// La zone de bytes
	BYTE* current;	// Pointeur fichier

public:
	BinMemFile(long size);									// size : taille du fichier mémoire en bytes
	~BinMemFile();
	void rewind();											// Repositionne le pointeur current au début du fichier
	void fseek(long offset);								// Positionne le pointeur current au déplacement offset à partir du début
	void fread(void* buf, binMemFileMode mode, int num);	// Lit num bytes ou dwords (selon mode) à partir de buffer et les écrit dans buf. (!) Pas de controle effectué sur la taille de buf
	void fwrite(const void* buf, binMemFileMode mode, int num);	// Lit num bytes ou dwords (selon mode) à partir de buf et les écrit dans buffer. (!) Pas de controle effectué sur la taille de buf
};



#endif // !defined
