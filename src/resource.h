//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by bl4viewer.rc
//
#define IDC_MYICON                      2
#define IDD_BL4VIEWER_DIALOG            102
#define IDD_ABOUTBOX                    103
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDS_HELLO                       106
#define IDI_BL4VIEWER                   107
#define IDI_SMALL                       108
#define IDC_BL4VIEWER                   109
#define IDR_MAINFRAME                   128
#define IDD_MAINBOX                     129
#define IDVIEW                          1000
#define IDABOUT                         1001
#define IDOPEN                          1002
#define IDOPENBL4                       1002
#define IDC_BL4NAME                     1003
#define IDOPENGHT                       1004
#define IDC_FULLSCREEN                  1005
#define IDC_GHTNAME                     1006
#define IDC_STATICGHT                   1007
#define IDC_STATICBL4                   1008
#define IDC_TRACES                      1009
#define IDC_STATICTRACES                1010
#define IDOPENGHT2                      1011
#define IDC_GHTNAME2                    1012
#define IDOPENGHT3                      1013
#define IDC_GHTNAME3                    1014
#define IDOPENGHT4                      1017
#define IDC_GHTNAME4                    1018
#define IDOPENGHT5                      1019
#define IDC_GHTNAME5                    1020
#define IDC_MODE                        1029
#define IDC_ADAPTER                     1030
#define IDC_STATIC                      -1

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
