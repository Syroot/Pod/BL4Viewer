//**************************************************************************
//* struct3d.h	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Janvier 2003
//*
//* Définition des structures utilisées pour la 3D
//***************************************************************************


#ifndef __STRUCT3D_H
#define __STRUCT3D_H


struct POINT3D
{
	float x, y, z;
};

struct POINT3DCOL
{
	int x, y, z;
	unsigned int color;
};

struct INDEXEDVERTEX
{
	int index;			// Index vers un POINT3DCOL
	float u,v;
	int indexTexture;
	bool visible;		// Texture visible ou non
	bool isTri;			// Appartient à un triangle ou à un quadrilatère
};

struct VERTEXBUF
{
	float x, y, z;
	unsigned int color;
	float u,v;
};

#endif // __STRUCT3D_H
