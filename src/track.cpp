//**************************************************************************
//* track.h	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Définition des fonctions de gestion d'un track
//***************************************************************************


#include "track.h"

using namespace std;

Track::Track()
{
	bl4Path = NULL;
	bl4Name = NULL;
	bl4TexturesPath = NULL;
	bl4File = NULL;
	numVB = 0;
	vBNumVertices = NULL;
	iBNumIndices = NULL;
	vertexBuffer = NULL;
	indexBuffer = NULL;
	textures = NULL;
}

/************************************************************************/

Track::~Track()
{
	if (iBNumIndices)  { delete[] iBNumIndices;  iBNumIndices  = NULL;}
	if (vBNumVertices) { delete[] vBNumVertices; vBNumVertices = NULL;}
	if (vertexBuffer) {
		for (int i=0; i<numVB; i++) {
			if (vertexBuffer[i]) { vertexBuffer[i]->Release(); }
		}
		delete[] vertexBuffer;
		vertexBuffer = NULL;
	}
	if (indexBuffer) {
		for (int i=0; i<numVB; i++) {
			if (indexBuffer[i]) { indexBuffer[i]->Release(); }
		}
		delete[] indexBuffer;
		indexBuffer = NULL;
	}
	if (textures) {
		for (int i=0; i<numVB; i++) {
			if (textures[i]) { textures[i]->Release(); }
		}
		delete[] textures;
		textures= NULL;
	}
}

/************************************************************************/

void Track::Draw(IDirect3DDevice9 *dev) const
{
	dev->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0));
	dev->SetVertexShader(NULL);
	for (int i=0; i<numVB; i++) {
		if (vertexBuffer[i]) {
			if (dev->SetTexture(0, textures[i]) != D3D_OK) exit(0);
			dev->SetStreamSource(0, vertexBuffer[i], 0, sizeof(VERTEXBUF));
			dev->SetIndices(indexBuffer[i]);
			dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vBNumVertices[i], 0, iBNumIndices[i] / 3);
		}
	}
}

/************************************************************************/

bool Track::LoadTrack(char* path, char* Name, char* texturesPath, IDirect3DDevice9 *dev)
{
	bl4Path = path;
	bl4Name= Name;
	bl4TexturesPath = texturesPath;

	if (!DecryptFile()) { return false;	}

	unsigned char b;
	char str[16];
	int numTextures, numSoustextures, isCryptString, numSectors, i;

	// cherche trackname.rte
	strcpy_s(str,bl4Name);
	_strupr_s(str);
	if (strcmp(str,"ARCADE")) {
		// Cas général
		xorFile->fseek(0x184);
		xorFile->fread(&b,BMF_BYTE,1);
		switch (b) {
		case 0:  xorFile->fseek(0x577);break;
		case 4:  xorFile->fseek(0x6af);break;
		default: xorFile->fseek(0x57b);break;
		}
	} else {
		// Pour Arcade
		xorFile->fseek(0x653);
	}

	ReadString();												// trackname.rte
	ReadDWord(16);												// Réservation mémoire
	ReadString();												// Nom du projet interne
	// Extraction Textures
	xorFile->fread(&numTextures,BMF_DWORD,1);					// Nombre de textures
	numVB = numTextures;
	ReadDWord(1);												// $0
	for (i=0; i<numTextures; i++) {								// Description des sous textures
		xorFile->fread(&numSoustextures,BMF_DWORD,1);
		ReadDWord(13*numSoustextures);
	}
	if (!ReadTextures(numTextures)) return false;				// Description des textures
	// Extraction Blocs de polygônes et chargement
	xorFile->fread(&isCryptString,BMF_DWORD,1);					// $0 ou $1
	xorFile->fread(&numSectors,BMF_DWORD,1);					// Nombre de secteurs
	ReadSectors( numSectors, isCryptString, dev);				// Description des secteurs

	delete xorFile;
	xorFile = NULL;

	// Chargement des textures
	return LoadTextures(dev);
}

/************************************************************************/

bool Track::DecryptFile()
{
	int key=0X00000F7E, count = 1, dw;
	size_t numread;
	long size;

	if (fopen_s(&bl4File,bl4Path,"rb")) { return false;}
	fseek(bl4File, 0, SEEK_END);
	size = ftell(bl4File);
	rewind(bl4File);
	xorFile = new BinMemFile(size);
	numread=fread(&dw,4,1,bl4File);
	while (feof(bl4File)==0) {
		if (count==4096) {
			numread=fread(&dw,4,1,bl4File);
			count=1;
		}
		dw=dw^key;
		if (numread!=0)	xorFile->fwrite(&dw,BMF_DWORD,1);
		numread=fread(&dw,4,1,bl4File);
		count++;
	}
	fclose(bl4File);
	xorFile->rewind();
	return true;
}

/************************************************************************/

int Track::ReadString()
{
	int intLength = 0;
	unsigned char b, length;

	xorFile->fread(&length,BMF_BYTE,1);
	for (int i=0; i<length; i++) xorFile->fread(&b,BMF_BYTE,1);

	intLength = length;
	return intLength;
}

/************************************************************************/

void Track::ReadDWord(int numDW)
{
	int dw;
	for (int i=0; i<numDW; i++) xorFile->fread(&dw,BMF_DWORD,1);
}

/************************************************************************/

int Track::PixelConvert( int r5v6b5) const
{
	int r=0, v=0, b=0;

	b = (r5v6b5 & 0x0000001F) << 3;
	v = (r5v6b5 & 0x000007E0) << 5;
	r = (r5v6b5 & 0x0000F800) << 8;

	return (r | v | b);
}

/************************************************************************/

bool Track::ReadTextures(int num)
{
	FILE* outputFile;
	char str[512], stri[4];
	unsigned char b;
	int i, j, dw;

	for (i=0; i<num; i++) {
		// Ouverture du fichier de sortie
		strcpy_s(str, bl4TexturesPath);
		strcat_s(str, "\\");
		strcat_s(str, bl4Name);
		_itoa_s(i, stri, 10);
		strcat_s(str, stri);
		strcat_s(str, ".tga");
		if (fopen_s(&outputFile,str,"r+b")) {
			// Le fichier n'existe pas donc on le crée
			if (fopen_s(&outputFile,str,"wb")) {return false;}
			// Exportation
			// En-tête du TGA
			dw = 0x00020000;
			fwrite(&dw, 4, 1, outputFile);
			dw = 0;
			fwrite(&dw, 4, 1, outputFile);
			fwrite(&dw, 4, 1, outputFile);
			dw = 0x01000100	;					// 256*256
			fwrite(&dw, 4, 1, outputFile);
			b = 0x18;							// 24 bits
			fwrite(&b, 1, 1, outputFile);
			b = 0;
			fwrite(&b, 1, 1, outputFile);
			// Pixels
			for (j=0; j<65536; j++) {
				dw = 0;
				xorFile->fread(&dw, BMF_BYTE, 2);
				dw = PixelConvert(dw);
				fwrite( &dw, 3, 1, outputFile);
			}
		} else {
			// Passe la texture sans exporter
			for (j=0; j<65536; j++) {
				xorFile->fread(&dw, BMF_BYTE, 2);
			}
		}
		fclose(outputFile);
	}
	return true;
}

/************************************************************************/

bool Track::LoadTextures(IDirect3DDevice9 *dev)
{
	char str[512], stri[4];
	textures = new IDirect3DTexture9*[numVB];

	for (int i=0; i<numVB; i++) {
		strcpy_s(str, bl4TexturesPath);
		strcat_s(str, "\\");
		strcat_s(str, bl4Name);
		_itoa_s(i, stri, 10);
		strcat_s(str, stri);
		strcat_s(str, ".tga");
		if (D3DXCreateTextureFromFile(dev, str, &(textures[i])) != D3D_OK) return false;
	}

	return true;
}

/************************************************************************/

// Note : algo complexe car la facon d'indexer les vertices est différente entre le format bl4 et direct3d
void Track::ReadSectors(int num, int isCryptString, IDirect3DDevice9 *dev)
{
	unsigned char b;											// Stocke les bytes  lus à partir du fichier
	int dw;														// Stocke les dwords lus à partir du fichier
	int numVertices, numIndexedVertices, numPolygones, numFaces = 0, numTri, numQuadri, numSides, i, j, k;	// Compteurs
	int index, indexTex, base;									// Stockent des index
	bool visible;												// Indique si un sommet est texturé ou non

	// Structures de stockage de vertices indexés au format BL4
	POINT3DCOL* points;											// Stocke les points3Dcol d'un secteur (cad les coordonnées xyz d'un sommet et sa couleur)
	INDEXEDVERTEX* indexedVertices;								// Stocke les sommets d'un secteur avec référence d'index vers le tableau points

	// Structures de stockage de vertices indexés au format D3D
	vector<VERTEXBUF>* vertices = new vector<VERTEXBUF>[numVB];	// Tableau de meshs					( 1 mesh   (=vector de VERTEXBUF) par texture)
	vector<short>* indices = new vector<short>[numVB];			// Tableau de buffer d'index 32bits ( 1 buffer (=vector de short  )    par texture)

	VERTEXBUF vElement;											// Element qu'on ajoutera au vector vertices


	// Lecture des secteurs
	for (i=0; i<num; i++) {
		// ------------ Sommets --------------
		xorFile->fread(&numIndexedVertices,BMF_DWORD,1);	// Nb de sommets
		points = new POINT3DCOL[numIndexedVertices];
		for (j=0; j<numIndexedVertices; j++) {				// Coordonnées des sommets
			xorFile->fread(&(points[j].x),BMF_DWORD,1);
			xorFile->fread(&(points[j].y),BMF_DWORD,1);
			xorFile->fread(&(points[j].z),BMF_DWORD,1);
		}
		k=0;
		// ------------ Polygônes ------------
		xorFile->fread(&numPolygones,BMF_DWORD,1);				// Nb de poly
		xorFile->fread(&numTri,BMF_DWORD,1);					// Nb de tri
		xorFile->fread(&numQuadri,BMF_DWORD,1);					// Nb de quadri
		numVertices = 3*numTri + 4*numQuadri;
		indexedVertices = new INDEXEDVERTEX[numVertices];
		for (j=0; j<numPolygones; j++) {
			if (isCryptString) ReadString();					// Nom du poly
			xorFile->fread(&dw,BMF_DWORD,1);					// Nombre de côtés
			if (dw==3) {
				// Cas d'un triangle
				xorFile->fread(&(indexedVertices[k].index),BMF_DWORD,1);
				xorFile->fread(&(indexedVertices[k+1].index),BMF_DWORD,1);
				xorFile->fread(&(indexedVertices[k+2].index),BMF_DWORD,1);
				ReadDWord(4);									// Index inutile + normale (XYZ)
				if (ReadString() == 6) {						// TEXGOU / GOURAUD / FLAT
					visible = true;
					xorFile->fread(&dw,BMF_DWORD,1);			// Index texture
				} else {
					visible = false;
					xorFile->fread(&dw,BMF_DWORD,1);
					dw = 0;
				}
				indexedVertices[k].visible = visible;
				indexedVertices[k+1].visible = visible;
				indexedVertices[k+2].visible = visible;
				indexedVertices[k].isTri = true;
				indexedVertices[k+1].isTri = true;
				indexedVertices[k+2].isTri = true;
				indexedVertices[k].indexTexture = dw;
				indexedVertices[k+1].indexTexture = dw;
				indexedVertices[k+2].indexTexture = dw;
				xorFile->fread(&dw,BMF_DWORD,1);				// Coordonnées de mapping
				indexedVertices[k].u = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k].v = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+1].u = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+1].v = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+2].u = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+2].v = ((float) dw) / 255;
				ReadDWord(5);									// 3 DW inconnus + 2 DW options
				k+=3;
				numFaces++;
			} else {
				// Cas d'un quadrilatère
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k].index = dw;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+1].index = dw;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+2].index = dw;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+3].index = dw;
				ReadDWord(3);									// Normale (XYZ)
				if (ReadString() == 6) {						// TEXGOU / GOURAUD / FLAT
					visible = true;
					xorFile->fread(&dw,BMF_DWORD,1);			// Index texture
				} else {
					visible = false;
					xorFile->fread(&dw,BMF_DWORD,1);
					dw = 0;
				}
				indexedVertices[k].visible = visible;
				indexedVertices[k+1].visible = visible;
				indexedVertices[k+2].visible = visible;
				indexedVertices[k+3].visible = visible;
				indexedVertices[k].isTri = false;
				indexedVertices[k+1].isTri = false;
				indexedVertices[k+2].isTri = false;
				indexedVertices[k+3].isTri = false;
				indexedVertices[k].indexTexture = dw;
				indexedVertices[k+1].indexTexture = dw;
				indexedVertices[k+2].indexTexture = dw;
				indexedVertices[k+3].indexTexture = dw;
				xorFile->fread(&dw,BMF_DWORD,1);				// Coordonnées de mapping
				indexedVertices[k].u = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k].v = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+1].u = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+1].v = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+2].u = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+2].v = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+3].u = ((float) dw) / 255;
				xorFile->fread(&dw,BMF_DWORD,1);
				indexedVertices[k+3].v = ((float) dw) / 255;
				ReadDWord(6);									// 4 DW inconnus + 2 DW options
				k+=4;
				numFaces += 2;
			}
		}  // Fin boucle de polygones

		// -----  Normale des sommets et Rayon du bloc  -------
		ReadDWord(3*numIndexedVertices + 1);

		// ----------  Luminescence  ----------
		for (j=0; j<numIndexedVertices; j++) {
			xorFile->fread(&b,BMF_BYTE,1);
			points[j].color = D3DCOLOR_ARGB(255, b, b, b);
		}

		// ----------  Espace du bloc  --------
		ReadDWord(6);

		// Recombinaison des PONT3DCOL et INDEXEDVERTEX (BL4) pour obtenir des VERTEX indexés (D3D) triés par texture
		// On place les vertices de même texture ensemble et on construit les indexbuffers en faisant attention au cas des triangles et des quadrilatères
		j = 0;
		while( j<numVertices) {
			indexTex = indexedVertices[j].indexTexture;			// Numéro de texture => permet de savoir quels vertexbuf et indexbuf mettre à jour en y ajoutant le polygone
			base = (int)vertices[indexTex].size();					// Permet de calculer les index
			if (indexedVertices[j].isTri) {
				numSides = 3;
				// Construction de l'index pour un triangle
				indices[indexTex].push_back(base);
				indices[indexTex].push_back(base+1);
				indices[indexTex].push_back(base+2);
			} else {
				numSides = 4;
				// Construction de l'index pour un quadrilatère
				indices[indexTex].push_back(base);
				indices[indexTex].push_back(base+1);
				indices[indexTex].push_back(base+2);
				indices[indexTex].push_back(base+2);
				indices[indexTex].push_back(base+3);
				indices[indexTex].push_back(base);
			}
			// On ajoute les sommets (3 ou 4) d'un même polygone d'un coup
			for (k=0; k<numSides; k++) {
				// Construction du VERTEXBUF
				index = indexedVertices[j+k].index;
				vElement.x =   (float) (((float) points[index].x) / 6553600);
				vElement.y =   (float) (((float) points[index].y) / 6553600);
				vElement.z = - (float) (((float) points[index].z) / 6553600);
				if (indexedVertices[j+k].visible) {
					vElement.color = points[index].color;
				} else {
					vElement.color = 0;
				}
				vElement.u = indexedVertices[j+k].u;
				vElement.v = 1 - indexedVertices[j+k].v;
				// Ajout du nouveau VERTEXBUF au vector correspondant à sa texture
				vertices[indexTex].push_back(vElement);
			}
			j += numSides;
		}	// Fin boucle de recombinaison

		delete[] points;
		delete[] indexedVertices;

	} // Fin boucle de secteurs


	// Création et remplissage des vertexbuffers et indexbuffers (simple recopie)
	indexBuffer =  new IDirect3DIndexBuffer9* [numVB];				// Les indexBuffer
	vertexBuffer = new IDirect3DVertexBuffer9*[numVB];				// Les vertexBuffer
	VERTEXBUF* v;													// Aide au remplissage des VB
	short* id;														// Aide au remplissages des IB
	vector<VERTEXBUF>::const_iterator verticesIt;					// Itérateur pour vertices
	vector<short>::const_iterator indicesIt;						// Itérateur pour les indices

	vBNumVertices = new int[numVB];									// cf header
	iBNumIndices = new int[numVB];


	for (i=0; i<numVB; i++) {
		if (vertices[i].empty()) {
			vertexBuffer[i] = NULL;
			indexBuffer[i] = NULL;
		} else {
			// MAJ du nombre de vertices et d'indices des VB et ID
			vBNumVertices[i] = (int)vertices[i].size();
			iBNumIndices[i]  = (int)indices[i].size();

			// Création de l'IB
			dev->CreateIndexBuffer((UINT)(sizeof(short) * indices[i].size()),
				D3DUSAGE_WRITEONLY,
				D3DFMT_INDEX16,
				D3DPOOL_DEFAULT,
				&(indexBuffer[i]),
				NULL);

			// Remplissage de l'IB
			indexBuffer[i]->Lock(0, (UINT)(sizeof(short) * indices[i].size()), (void**)&id, 0);
			indicesIt = indices[i].begin();
			j = 0;
			while (indicesIt != indices[i].end()) {
				id[j] = *indicesIt;
				indicesIt++;
				j++;
			}
			indexBuffer[i]->Unlock();

			// Création du VB
			dev->CreateVertexBuffer((UINT)(sizeof(VERTEXBUF) * vertices[i].size()),
			D3DUSAGE_WRITEONLY, D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE2(0),
			D3DPOOL_DEFAULT,
			&(vertexBuffer[i]),
			NULL);

			// Remplissage du VB
			vertexBuffer[i]->Lock(0, 0, (void**)&v, 0);
			verticesIt = vertices[i].begin();
			j = 0;
			while (verticesIt != vertices[i].end()) {
				v[j].x = verticesIt->x;
				v[j].y = verticesIt->y;
				v[j].z = verticesIt->z;
				v[j].color = verticesIt->color;
				v[j].u = verticesIt->u;
				v[j].v = verticesIt->v;
				verticesIt++;
				j++;
			}
			vertexBuffer[i]->Unlock();

		} // Fin si vertices[i] n'est pas vide
	} // Fin boucle de VB

	// Maj des chaines à afficher
	_itoa_s(numFaces, strNumFaces, 10);
	strcat_s(strNumFaces, " faces");
	_itoa_s(numVB, strNumTextures, 10);
	strcat_s(strNumTextures, " textures");

	// Libération finale
	for (i=0; i<numVB; i++) {
		vertices[i].clear();
		indices[i].clear();
	}
	delete[] vertices;
	delete[] indices;
}
