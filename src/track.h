//**************************************************************************
//* track.h	- bl4 Viewer
//*
//* Picpic
//*
//* 16 Février 2003
//*
//* Gére un track
//***************************************************************************


#ifndef __TRACK_H
#define __TRACK_H

#include <d3d9.h>
#include <d3dx9.h>
#include "stdio.h"
#include "struct3d.h"
#include "misc.h"
#include <vector>
#include <list>

class Track {
public:
	Track();
	~Track();

	bool LoadTrack( char* path, char* name, char* texturesPath, IDirect3DDevice9 *dev); // Charge un fichier bl4 et crée les vertexbuffers, textures etc.
	void Draw(IDirect3DDevice9 *dev) const;												// Affichage du track

	// Méthodes d'accès
	inline char* GetNumFaces() 		{ return strNumFaces; }
	inline char* GetNumTextures() 	{ return strNumTextures; }

private:
	// Méthodes privées
	bool DecryptFile();							// Décrypte le fichier ( crée le fichier temporaire textures/bl4view.nfo)
	int  ReadString();							// Lit une chaine cryptée (retourne sa longueur)
	void ReadDWord(int numDW);					// Lit numDW DWords
	bool ReadTextures(int num);					// Lit et exporte num textures
	int  PixelConvert ( int r5v6b5) const;		// Convertit un pixel R5V6B5 en R8V8B8
	bool LoadTextures(IDirect3DDevice9 *dev);	// Charge les textures en mémoire
	void ReadSectors(int num, int isCryptString, IDirect3DDevice9 *dev);	// Lit et exporte num secteurs. isCryptString indique si les string cryptées du format bl4 sont présentes ou non dans la description des polygônes.

	// Champs
	char* bl4Path;					// Chemin complet du bl4
	char* bl4Name;					// Nom du bl4
	char* bl4TexturesPath;			// Chemin complet des textures
	FILE* bl4File;					// Fichier bl4
	BinMemFile* xorFile;			// Fichier décrypté en mémoire
	int numVB;						// Nombre de textures et donc de vertexbuffer
	int *vBNumVertices;				// Stocke le nombre de sommets des VB
	int *iBNumIndices;				// Stocke le nombre d'indices des IB
	char strNumFaces[32];			// Pour l'affichage du nombre de faces
	char strNumTextures[32];		// Pour l'affichage du nombre de textures

	IDirect3DIndexBuffer9 **indexBuffer;		// IndexBuffers des VB du circuit (1 par texture)
	IDirect3DVertexBuffer9 **vertexBuffer;		// VB constituant le circuit (1 par texture)
	IDirect3DTexture9 **textures;				// Textures du circuit
};

#endif // __TRACK_H
